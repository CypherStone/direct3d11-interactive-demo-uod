#pragma once
#include "stdafx.h"

class Camera
{
	private:
		FLOAT _fOV;
		FLOAT _aspectRatio;
		FLOAT _nearView;
		FLOAT _farView;
		UINT _screenWidth;
		UINT _screenHeight;
		D3DXVECTOR3 _position;
		D3DXVECTOR3 _angles;
		D3DXMATRIX _view;
		D3DXMATRIX baseView;
		D3DXMATRIX _projection;
		D3D11_VIEWPORT _viewport;
		D3DXVECTOR3 character;

		void Copy(const Camera& cam);
		void Init(void);
		void SetProjection(void);

	public:
		Camera(void);
		~Camera(void);

		Camera& operator=(const Camera &rhs);

		void Initiate(UINT width, UINT height, FLOAT fOV, FLOAT nearView, FLOAT farView, D3DVECTOR position, D3DVECTOR angles);

		void BackwardMovement(float speed);
		void ForwardMovement(float speed);
		void StrafeLeft(float speed);
		void StrafeRight(float speed);
		void InclineHeight(float speed);
		void DeclineHeight(float speed);

		void Update(DirectInputManager *inputManager);
		void GenerateBaseView(void);

		inline void SetScreen(UINT width, UINT height){
			_screenWidth = width;
			_screenHeight = height;
			
			_aspectRatio = (float)width/(float)height;
			
			_viewport.TopLeftX = 0;
			_viewport.TopLeftY = 0;
			_viewport.Width = (float)ScreenWidth();
			_viewport.Height = (float)ScreenHeight();
			_viewport.MinDepth = 0.0f;
			_viewport.MaxDepth = 1.0f;
			
			SetProjection();
		}

		inline void SetFOV(FLOAT fOV){
			_fOV = fOV;
			SetProjection();
		}

		inline void XAngle(FLOAT x){
			_angles.x = x;
		}		
		inline void YAngle(FLOAT y){
			_angles.y = y;
		}
		inline void ZAngle(FLOAT z){
			_angles.z = z;
		}	

		inline void XPosition(FLOAT x){
			_position.x = x;
		}	
		inline void YPosition(FLOAT y){
			_position.y = y;
		}
		inline void ZPosition(FLOAT z){
			_position.z = z;
		}

		inline D3D11_VIEWPORT GetViewport(void)	{
			return _viewport;
		}

		inline UINT ScreenWidth(void){
			return _screenWidth;
		}
		inline UINT ScreenHeight(void){
			return _screenHeight;
		}

		inline D3DXMATRIX& ViewMatrix(void){
			return _view;
		}

		inline D3DXMATRIX& ProjectionMatrix(void){
			return _projection;
		}

		inline D3DXVECTOR3 Position(void){
			return _position;
		}

		inline void PositionVelocity(D3DXVECTOR3 velocity){
			_position += velocity;
		}

		inline D3DXVECTOR3 XYZAngles(void){
			return _angles;
		}

		inline D3DXMATRIX GetBaseView(void){
			return baseView;
		}

		inline D3DXVECTOR3 CharacterPosition(void){
			D3DXMATRIX rotationMatrix;
			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, -_angles.y, 0, 0);
			D3DXVECTOR3 temp(0.0f, -55.0f, 50.0f);
			D3DXVec3TransformCoord(&character, &temp, &rotationMatrix);
			character += _position;
			return character;
		}

		inline D3DXVECTOR3 ObjectOffset(D3DXVECTOR3 temp){
			D3DXMATRIX rotationMatrix;
			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, -_angles.y, 0, 0);
			D3DXVec3TransformCoord(&character, &temp, &rotationMatrix);
			character += _position;
			return character;
		}

};