#include "stdafx.h"

DirectLight::DirectLight(void)
{
	Init();
}

DirectLight::DirectLight(const DirectLight &light)
{
	Copy(light);
}

DirectLight::~DirectLight(void){ }

void DirectLight::SetupLight(D3DXVECTOR4 amb, D3DXVECTOR4 diff, D3DXVECTOR4 spec, float specPow, D3DXVECTOR3 pos, D3DXVECTOR3 emis, float distance)
{
	ambient = amb;
	diffuse = diff;
	specular = spec;
	specularPower = specPow;
	position = pos;
	emissive = emis;
	lightDistanceSquared = distance;
	lightOn = true;
}

bool DirectLight::SetupShadowMap(ID3D11Device *device, int width, int height, D3DXVECTOR2 screen)
{
	shadowMapHeight = height;
	shadowMapWidth = width;
	shadowMap = new DirectRenderTarget;
	if(!shadowMap->SetupRenderTarget(device, width, height, screen))
		return false;

	return true;
}

D3DXVECTOR4 DirectLight::GetAmbient(void)
{
	return ambient;
}

D3DXVECTOR4 DirectLight::GetDiffuse(void)
{
	return diffuse;
}

D3DXVECTOR4 DirectLight::GetSpecular(void)
{
	return specular;
}

D3DXVECTOR3 DirectLight::GetPosition(void)
{
	return position;
}

float DirectLight::GetSpecularPower(void)
{
	return specularPower;
}

D3DXVECTOR3 DirectLight::GetEmissive(void)
{
	return emissive;
}

float DirectLight::GetLightDistance(void)
{
	return lightDistanceSquared;
}

void DirectLight::SetAmbient(float red, float green, float blue, float alpha)
{
	ambient = D3DXVECTOR4(red, green, blue, alpha);
}

void DirectLight::SetDiffuse(float red, float green, float blue, float alpha)
{
	diffuse = D3DXVECTOR4(red, green, blue, alpha);
}

void DirectLight::SetSpecular(float red, float green, float blue, float alpha)
{
	specular = D3DXVECTOR4(red, green, blue, alpha);
}

void DirectLight::SetPosition(float x, float y, float z)
{
	position = D3DXVECTOR3(x, y, z);
}

void DirectLight::SetSpecularPower(float power)
{
	specularPower = power;
}

void DirectLight::GenerateProjection(float screenDepth, float screenNear)
{
	float fieldOfView, screenAspect;

	fieldOfView = (float)D3DX_PI / 2.0f;
	screenAspect = 1.0f;

	D3DXMatrixPerspectiveFovLH(&projection, fieldOfView, screenAspect, screenNear, screenDepth);
}
	
void DirectLight::GenerateView(void)
{
	D3DXVECTOR3 up;

	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	D3DXMatrixLookAtLH(&view, &position, &lookat, &up);
}

void DirectLight::Copy(const DirectLight &light)
{
	ambient = light.ambient;
	diffuse = light.diffuse;
	specular = light.specular;
	position = light.position;
	specularPower = light.specularPower;
	emissive = light.emissive;
	lightDistanceSquared = light.lightDistanceSquared;
	view = light.view;
	projection = light.projection;
	shadowMap = light.shadowMap;
	lightOn = light.lightOn;
	shadowMapHeight = light.shadowMapHeight;
	shadowMapWidth = light.shadowMapWidth;
}

void DirectLight::Init(void)
{
	SecureZeroMemory(&ambient, sizeof(D3DXVECTOR4));
	SecureZeroMemory(&diffuse, sizeof(D3DXVECTOR4));
	SecureZeroMemory(&specular, sizeof(D3DXVECTOR4));
	SecureZeroMemory(&position, sizeof(D3DXVECTOR3));
	specularPower = 0.0f;
	shadowMap = 0;
}