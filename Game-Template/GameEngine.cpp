#include "stdafx.h"

void GameEngine::Initiate(void)
{
	hWnd = NULL;
	d3DDevice = 0;
	renderEngine = 0;
	inputManager = 0;
	directDebug = 0;
	player = 0;
	lights = 0;
	collisions = 0;
	billboards = 0;
	doorA = 0;
	doorB = 0;
	movingPlat = 0;
	direction = true;
	gameRunning = false;
	fog = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	fogDistance = D3DXVECTOR2(1200.0f, 1500.0f);
	fogChange = true;
	states = States::Menu;
	renderDebugText = false;
	suitAngle = 0.0f;
	suitYAngle = 150.0f;
	suitXAngle = 50.0f;
	rotatingFloor = 0;
	suit = 0;
}

GameEngine::GameEngine(void)
{
	Initiate();
}

GameEngine::GameEngine(const GameEngine &game) { }
		
GameEngine::~GameEngine(void) { }

bool GameEngine::SetupEngine(HINSTANCE hInstance, HWND hwnd, UINT width, UINT height)
{
	int shadowMapHeight = 1024;
	int shadowMapWidth = 1024;

	hWnd = hwnd;
	renderType = RenderEngine::SOFTSHADOWMAP;
	
	player = new Player;

	if(player)
	{
		player->SetupPlayer();
		player->GetCamera()->Initiate(width, height, 45.0f, 1.0f, 2500.0f, D3DXVECTOR3(0.0f, 0.0f, -10.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		player->GetCamera()->GenerateBaseView();
		player->GetCamera()->ZPosition(1400.0f);
		player->GetCamera()->YAngle((float)(180 * 3.14 / 180));
	}
	else
		return false;

	// Setup the Direct3D Manager.
	d3DDevice = new Direct3DManager;
	if(d3DDevice)
	{
		if(!d3DDevice->SetupDirectX11(hWnd, *player->GetCamera(), true, false))	
			return false;
	}
	else 
		return false;
	
	renderEngine = new RenderEngine;
	if(renderEngine)
	{
		if(!renderEngine->SetupEngine(d3DDevice, 1024, 1024))
			return false;
	}
	else
		return false;

	// Setup the input manager for the engine.
	inputManager = new DirectInputManager;
	if(inputManager)
	{
		if(!inputManager->SetupInput(hInstance, hWnd, width, height)) 
			return false;
	}
	else 
		return false;

	directDebug = new DirectDebug;
	if(directDebug)
	{
		directDebug->SetupDisplayFont(d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	}
	else 
		return false;
	
	// Loading all of the .OBJ 3D models.
	LoadModels();

	// Finally, setting up all of the lights that are in the tech demo's scene.
	SetupLights();

	menu.InitMenu(d3DDevice->GetDevice());
	pause.InitMenu(d3DDevice->GetDevice());

	return true;
}

void GameEngine::LoadModels(void)
{
	int models;
	const int objAmount = 8;
	objects = new std::vector<Object3D>;
	transObjects = new std::vector<Object3D>;
	billboards = new std::vector<BillboardObject>;
	buttons = new std::vector<ButtonObject>;
	pickups = new std::vector<PickupObject>;
	doorA = new Object3D;
	doorB = new Object3D;
	movingPlat = new Object3D;
	suit = new Object3D;
	rotatingFloor = new Object3D;

	//Loads models relative to the project from the string array
	std::string objLocation[objAmount] = 
	{
		".\\Models\\GameMap\\map.obj",
		".\\Models\\GameMap\\metal-rack.obj",
		".\\Models\\GameMap\\auto-door.obj",
		".\\Models\\GameMap\\bigscreen.obj",
		".\\Models\\GameMap\\column.obj",
		".\\Models\\GameMap\\billboardroom.obj",
		".\\Models\\GameMap\\windowframe.obj",
		".\\Models\\GameMap\\platform.obj"
	};

	Utility::ImportOBJ(*player->GetCharacter(), ".\\Models\\GameMap\\clank\\clank.obj", d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());

	for(models = 0; models < objAmount; models++)
	{
		Object3D obj;

		if(models == 1)
		{
			Utility::ImportOBJ(obj, objLocation[models].c_str(), d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
			obj.SetPosition(D3DXVECTOR3(0, 100.0f, 1200.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(0, 100.0f, 900.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(0, 100.0f, 0.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(0, 100.0f, -300.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(0, 100.0f, -600.0f));
			objects->push_back(obj);
		}
		else if(models == 2)
		{	
			Utility::ImportOBJ(obj, objLocation[models].c_str(), d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
			doorB->CopyObject(obj);
			doorB->SetPosition(D3DXVECTOR3(45.0f, -172.5f, 317.5f));
			doorA->CopyObject(obj);
			doorA->SetPosition(D3DXVECTOR3(-45.0f, -172.5f, 317.5f));
			doorAnim = 45.0f;
		}
		else if(models == 3)
		{
			Utility::ImportOBJ(obj, objLocation[models].c_str(), d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
			obj.SetPosition(D3DXVECTOR3(0, -270.0f, -860.0f));
			objects->push_back(obj);
		}
		else if(models == 4)
		{
			Utility::ImportOBJ(obj, objLocation[models].c_str(), d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
			obj.SetPosition(D3DXVECTOR3(-400.0f, 0.0f, 1350.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(400.0f, 0.0f, 1350.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(-400.0f, 0.0f, 1050.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(400.0f, 0.0f, 1050.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(-400.0f, 0.0f, 750.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(400.0f, 0.0f, 750.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(-400.0f, 0.0f, 450.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(400.0f, 0.0f, 450.0f));
			objects->push_back(obj);
		}
		else if(models == 7)
		{
			Utility::ImportOBJ(obj, objLocation[models].c_str(), d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
			obj.SetPosition(D3DXVECTOR3(200.0f, -100.0f, 1000.0f));
			objects->push_back(obj);
			movingPlat->CopyObject(obj);
			movingPlatpos = 200;
			movingPlat->SetPosition(D3DXVECTOR3(-movingPlatpos, -100.0f, 750.0f));
			obj.SetPosition(D3DXVECTOR3(200.0f, -50.0f, 450.0f));
			objects->push_back(obj);
			obj.SetPosition(D3DXVECTOR3(0.0f, 0.0f, 370.0f));
			objects->push_back(obj);
		}
		else
		{
			Utility::ImportOBJ(obj, objLocation[models].c_str(), d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
			obj.SetPosition(D3DXVECTOR3(0, 0, 0));
			objects->push_back(obj);
		}
	}

	Object3D window;
	Utility::ImportOBJ(window, ".\\Models\\GameMap\\window.obj", d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	window.SetPosition(D3DXVECTOR3(0, 0, 0));
	transObjects->push_back(window);

	Object3D boards;
	Utility::ImportOBJ(boards, ".\\Models\\GameMap\\billboard_trooper.obj", d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	int offset = 1;
	for(models = 1; models < 20; models++)
	{
		BillboardObject trooper_board;
		trooper_board.GetObject3D()->CopyObject(boards);
		trooper_board.GetPosition().y = -172.5f;
		
		if(models <= 10)
		{
			trooper_board.GetPosition().x = -950.0f;
			trooper_board.GetPosition().z = 225.0f - (125.0f * models);
		}
		else
		{
			trooper_board.GetPosition().x = -800.0f;
			trooper_board.GetPosition().z = 300.0f - (125.0f * offset);
			offset++;
		}

		billboards->push_back(trooper_board);
	}


	Utility::ImportOBJ(*suit, ".\\Models\\GameMap\\clank\\clank.obj", d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	suit->SetPosition(D3DXVECTOR3(250, -170.0f, -250));
	Utility::ImportOBJ(*rotatingFloor, ".\\Models\\GameMap\\turningstand.obj", d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	rotatingFloor->SetPosition(D3DXVECTOR3(250, -250.0f, -250));
	Utility::ImportOBJ(boards, ".\\Models\\GameMap\\billboard_vader.obj", d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	BillboardObject vader_board;
	vader_board.GetObject3D()->CopyObject(boards);
	vader_board.GetPosition().x = -650.0f;
	vader_board.GetPosition().y = -172.5f;
	vader_board.GetPosition().z = -350.0f;
	billboards->push_back(vader_board);

	Object3D buttonObj;
	Utility::ImportOBJ(buttonObj, ".\\Models\\GameMap\\button.obj", d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	ButtonObject button;
	button.GetObject3D()->CopyObject(buttonObj);
	button.GetObject3D()->SetPosition(D3DXVECTOR3(0.0f, 75.0f, 320.0f));
	button.SetType(ButtonObject::ButtonType::OneHit);
	buttons->push_back(button);
	ButtonObject button2;
	button2.GetObject3D()->CopyObject(buttonObj);
	button2.GetObject3D()->SetPosition(D3DXVECTOR3(-395.0f, -190.0f, 50.0f));
	button2.GetObject3D()->YAngle((float)(90 * 3.14 / 180));
	button2.SetType(ButtonObject::ButtonType::Pushable);
	buttons->push_back(button2);
	ButtonObject buttonCube;
	buttonCube.GetObject3D()->CopyObject(buttonObj);
	buttonCube.GetObject3D()->SetPosition(D3DXVECTOR3(-395.0f, -190.0f, 900.0f));
	buttonCube.GetObject3D()->YAngle((float)(90 * 3.14 / 180));
	buttonCube.SetType(ButtonObject::ButtonType::OneHit);
	buttons->push_back(buttonCube);
	ButtonObject rotationButton;
	rotationButton.GetObject3D()->CopyObject(buttonObj);
	rotationButton.GetObject3D()->SetPosition(D3DXVECTOR3(300.0f, -190.0f, 50.0f));
	rotationButton.GetObject3D()->YAngle((float)(-90 * 3.14 / 180));
	rotationButton.SetType(ButtonObject::ButtonType::Pushable);
	buttons->push_back(rotationButton);

	Object3D cubeObj;
	Utility::ImportOBJ(cubeObj,".\\Models\\GameMap\\companion_cube.obj", d3DDevice->GetDevice(), d3DDevice->GetDeviceContext());
	PickupObject cube;
	cube.GetObject3D()->CopyObject(cubeObj);
	cube.GetObject3D()->SetPosition(D3DXVECTOR3(-200.0f, 50.0f, 750.0f));
	cube.GetObject3D()->YAngle((float)(45  * 3.14 / 180));
	pickups->push_back(cube);
	PickupObject cubeA;
	cubeA.GetObject3D()->CopyObject(cubeObj);
	cubeA.GetObject3D()->SetPosition(D3DXVECTOR3(-250.0f, 50.0f, -600.0f));
	cubeA.GetObject3D()->YAngle((float)(60 * 3.14 / 180));
	pickups->push_back(cubeA);
	PickupObject cubeB;
	cubeB.GetObject3D()->CopyObject(cubeObj);
	cubeB.GetObject3D()->SetPosition(D3DXVECTOR3(250.0f, 50.0f, -600.0f));
	cubeB.GetObject3D()->YAngle((float)(20 * 3.14 / 180));
	pickups->push_back(cubeB);

	collisions = new std::vector<DirectCollision>;
	DirectCollision collision;
	
	collision.SetMin(D3DXVECTOR3(objects->at(0).GetBounds().GetMin().x, 165, objects->at(0).GetBounds().GetMin().z));
	collision.SetMax(D3DXVECTOR3(objects->at(0).GetBounds().GetMax().x, 200, objects->at(0).GetBounds().GetMax().z));
	collisions->push_back(collision);

	collision.SetMin(D3DXVECTOR3(95,-200,300));
	collision.SetMax(D3DXVECTOR3(405,200,315));
	collisions->push_back(collision);
	
	collision.SetMin(D3DXVECTOR3(-405,-200,300));
	collision.SetMax(D3DXVECTOR3(-90,200,315));
	collisions->push_back(collision);

	collision.SetMin(D3DXVECTOR3(-90,-75,300));
	collision.SetMax(D3DXVECTOR3(90, 200,315));
	collisions->push_back(collision);
}

void GameEngine::SetupLights(void)
{
	lights = new std::vector<DirectLight>;
	
	DirectLight light;
	light.SetupLight(D3DXVECTOR4(0.3f, 0.3f, 0.3f, 1.0f), D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f),
		D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f), 64.0f, D3DXVECTOR3(0.0f, -210.0f, 1650.0f), D3DXVECTOR3(1.0f, 1.0f, 1.0f), 100.0f);
	light.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1000.0f, 1.0f));
	light.SetLookat(0.0f, 100.0f, 250.0f);
	light.GenerateProjection(1500.0f, 1.0f);
	light.GenerateView();
	lights->push_back(light);

	DirectLight lighta;
	lighta.SetupLight(D3DXVECTOR4(0.3f, 0.3f, 0.3f, 1.0f), D3DXVECTOR4(0.6f, 0.6f, 1.0f, 1.0f),
		D3DXVECTOR4(0.6f, 0.6f, 1.0f, 1.0f), 64.0f, D3DXVECTOR3(0.0f, 100.0f, 300.0f), D3DXVECTOR3(1.0f, 1.0f, 1.0f), 100.0f);
	lighta.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1000.0f, 1.0f));
	lighta.SetLookat(0.0f, 80, -50.1f);
	lighta.GenerateProjection(1500.0f, 1.0f);
	lighta.GenerateView();
	lights->push_back(lighta);

	DirectLight lightb;
	lightb.SetupLight(D3DXVECTOR4(0.3f, 0.3f, 0.3f, 1.0f), D3DXVECTOR4(1.0f, 0.4f, 0.4f, 1.0f),
		D3DXVECTOR4(1.0f, 0.4f, 0.4f, 1.0f), 64.0f, D3DXVECTOR3(0.0f, 100.0f, -850.0f), D3DXVECTOR3(1.0f, 1.0f, 1.0f), 100.0f);
	lightb.SetupShadowMap(d3DDevice->GetDevice(), 1024, 1024, D3DXVECTOR2(1000.0f, 1.0f));
	lightb.SetLookat(0.0f, -100.0f, -500.0f);
	lightb.GenerateProjection(1500.0f, 1.0f);
	lightb.GenerateView();
	lightb.SetLightOn(false);
	lights->push_back(lightb);

}

void GameEngine::GameLoop(void)
{
	MSG msg;
	gameRunning = true;
	UpdateGame();

	while(gameRunning)
    {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

		if(msg.message == WM_QUIT)
		{
			gameRunning = false;
		}
		else
		{
			directDebug->ComputeFrameRate();
			
			switch(states)
			{
				case States::Menu:
					{
						MenuMain::ReturnStates menuState = menu.UpdateMenu(inputManager);

						switch(menuState)
						{
							case MenuMain::ReturnStates::Game:
								{
									states = Game;
									menu.Delete();
								}
								break;
							case MenuMain::ReturnStates::ExitGame:
								{
									gameRunning = false;
									menu.Delete();
								}
								break;
							case MenuMain::ReturnStates::Menu:
								{
									d3DDevice->BeginRender((D3DXVECTOR4)fog);
									renderEngine->RenderDebug(false);
									RenderGame();
									menu.Render(d3DDevice);
									d3DDevice->EndRender();
								}
								break;
						}
					}
					break;
				case States::Game:
					{
						UpdateInput();
						UpdateGame();
						d3DDevice->BeginRender((D3DXVECTOR4)fog);
						renderEngine->RenderDebug(renderDebugText);
						RenderGame();
						d3DDevice->EndRender();
					}
					break;
				case States::Pause:
					{
						pauseMenu::ReturnStates pauseState = pause.UpdateMenu(inputManager);
						
						switch(pauseState)
						{
							case MenuMain::ReturnStates::Game:
								{
									states = Game;
								}
								break;
							case MenuMain::ReturnStates::ExitGame:
								{
									gameRunning = false;
									menu.Delete();
								}
								break;
							case MenuMain::ReturnStates::Menu:
								{
									d3DDevice->BeginRender((D3DXVECTOR4)fog);
									RenderGame();
									pause.Render(d3DDevice);
									d3DDevice->EndRender();
								}
								break;
						}
					}
					break;
			}
		}
	}
}

void GameEngine::UpdateGame(void)
{
	int i, count, collision;

	collision = (int) collisions->size();

	// Pass collision boxes to the player
	collisions->push_back(doorA->GetBounds());
	collisions->push_back(doorB->GetBounds());
	collisions->push_back(movingPlat->GetBounds());
	collisions->push_back(suit->GetBounds());
	collisions->push_back(rotatingFloor->GetBounds());

	int newCollisionSize = (int) collisions->size();;
	count = (int)pickups->size();
	std::vector<int> selfCollision;

	for(i = 0; i < count; i++)
	{
		selfCollision.push_back(newCollisionSize + i);
		collisions->push_back(pickups->at(i).GetObject3D()->GetBounds());
	}

	for(i = 0; i < count; i++)
	{
		pickups->at(i).Update(objects, collisions, selfCollision.at(i));
		pickups->at(i).PlayerUpdate(inputManager, player);
	}

	for(i = 0; i < count; i++)
	{
		collisions->pop_back();
	}

	for(i = 0; i < count; i++)
	{
		if(!pickups->at(i).IsHeld())
			collisions->push_back(pickups->at(i).GetObject3D()->GetBounds());
	}
			
	player->Update(inputManager, objects, collisions);

	collision = (int)collisions->size() - collision;
	for(i = 0; i < collision; i++)
	{
		collisions->pop_back();
	}

	// Billboards' rotation
	count = (int)billboards->size();
	for(i = 0; i < count; i++)
	{
		billboards->at(i).Update(player->GetCamera()->Position());
	}

	// Motion doors
	if(buttons->at(0).OneShot())
	{
		DirectCollision collision;		
		collision.SetMin(D3DXVECTOR3(-90,-200,200));
		collision.SetMax(D3DXVECTOR3(90,0,500));

		if(collision.IsColliding(player->GetCharacter()->GetBounds()))
		{
			if(doorAnim < 150.0f)
				doorAnim += 1.0f;

			doorA->SetPosition(D3DXVECTOR3(-doorAnim, -172.5f, 317.5f));
			doorB->SetPosition(D3DXVECTOR3(doorAnim, -172.5f, 317.5f));
		}
		else
		{
			bool stopDoors = false;
			count = (int)pickups->size();
			for(i = 0; i < count && !stopDoors; i++)
			{
				if(doorA->GetBounds().IsColliding(pickups->at(i).GetObject3D()->GetBounds()))
				{
					stopDoors = true;
				}
				if(doorB->GetBounds().IsColliding(pickups->at(i).GetObject3D()->GetBounds()))
				{
					stopDoors = true;
				}
			}

			if(!stopDoors)
			{
				if(doorAnim > 45.0f)
					doorAnim -= 1.0f;

				doorA->SetPosition(D3DXVECTOR3(-doorAnim, -172.5f, 317.5f));
				doorB->SetPosition(D3DXVECTOR3(doorAnim, -172.5f, 317.5f));
			}
		}

		lights->at(1).SetLightOn(true);
		lights->at(0).SetDiffuse(0.9f, 0.9f, 0.9f, 1.0f);
		lights->at(0).SetSpecular(0.9f, 0.9f, 0.9f, 1.0f);
	}
	else
	{
		buttons->at(0).Update(inputManager, &player->GetCharacter()->GetBounds());

		lights->at(1).SetLightOn(false);
		lights->at(0).SetDiffuse(0.5f, 0.6f, 0.5f, 1.0f);
		lights->at(0).SetSpecular(0.2f, 0.8f, 0.2f, 1.0f);
	}

	if(buttons->at(2).OneShot())
	{
		if (movingPlatpos > 200.0f)
		{
			direction = true;
		}
		if (movingPlatpos < -200.0f)
		{
			direction = false;
		}

		if( direction == true)
			movingPlatpos -= 1.0f;
		else
			movingPlatpos += 1.0f;

		movingPlat->SetPosition(D3DXVECTOR3(-movingPlatpos, -100.0f, 750.0f));
	}
	else
	{
		buttons->at(2).Update(inputManager, &player->GetCharacter()->GetBounds());
	}

	buttons->at(3).Update(inputManager, &player->GetCharacter()->GetBounds());
	
	if(buttons->at(3).Pressed())
	{
		buttons->at(3).GetObject3D()->SetPosition(D3DXVECTOR3(405.0f, -190.0f, 50.0f));

		suitAngle = (suitAngle < 360) ? suitAngle + 1.0f : 0.0f;
		suitYAngle = (suitYAngle < 360) ? suitYAngle + 1.0f : 0.0f;
		suitXAngle = (suitXAngle < 360) ? suitXAngle + 1.0f : 0.0f;
	}
	else
	{
		buttons->at(3).GetObject3D()->SetPosition(D3DXVECTOR3(400.0f, -190.0f, 50.0f));
	}

	suit->ZAngle((float)(suitYAngle * 3.14 / 180));
	suit->YAngle((float)(-suitAngle * 3.14 / 180));
	suit->XAngle((float)(suitXAngle * 3.14 / 180));
	rotatingFloor->YAngle((float)(-suitAngle * 3.14 / 180));

	buttons->at(1).Update(inputManager, &player->GetCharacter()->GetBounds());
	if(buttons->at(1).Pressed())
	{
		buttons->at(1).GetObject3D()->SetPosition(D3DXVECTOR3(-400.0f, -190.0f, 50.0f));
		lights->at(2).SetLightOn(true);
	}
	else
	{
		buttons->at(1).GetObject3D()->SetPosition(D3DXVECTOR3(-395.0f, -190.0f, 50.0f));
		lights->at(2).SetLightOn(false);
	}
}

void GameEngine::UpdateInput(void)
{
	if(inputManager->Update())
	{			
		if(inputManager->IsKeyDown(DIK_ESCAPE))
		{
			states = States::Pause;
			//gameRunning = false;
			return;
		}

		if(inputManager->IsKeyDown(DIK_NUMPADMINUS) && changeRenderMinus)
		{
			changeRenderMinus = false;
			if(renderType - 1 >= 0)
			{
				renderType = (RenderEngine::RenderType)((int)renderType - 1);
			}
		}
		else if(!inputManager->IsKeyDown(DIK_NUMPADMINUS) && !changeRenderMinus)
		{
			changeRenderMinus = true;
		}

		if(inputManager->IsKeyDown(DIK_NUMPADPLUS) && changeRenderPlus)
		{
			changeRenderPlus = false;
			if(renderType + 1 < sizeof(RenderEngine::RenderType) + 1)
			{
				renderType = (RenderEngine::RenderType)((int)renderType + 1);
			}
		}
		else if(!inputManager->IsKeyDown(DIK_NUMPADPLUS) && !changeRenderPlus)
		{
			changeRenderPlus = true;
		}

		if(inputManager->IsKeyDown(DIK_F12) && changeFullscreen)
		{
			d3DDevice->SetFullscreen();
			changeFullscreen = false;
		}
		else if(!inputManager->IsKeyDown(DIK_F12) && !changeFullscreen)
		{
			changeFullscreen = true;
		}

		if(inputManager->IsKeyDown(DIK_UP) && fogChange)
		{
			fogDistance = D3DXVECTOR2(600, 900);
			fogChange = false;
		}
		else if(inputManager->IsKeyDown(DIK_DOWN) && !fogChange)
		{
			fogDistance = D3DXVECTOR2(1200, 1500);
			fogChange = true;
		}

		if(inputManager->IsKeyDown(DIK_F1))
		{
			if(f1Fix)
			{
				renderDebugText = !renderDebugText;
				f1Fix = false;
			}
		}
		else
		{
			f1Fix = true;
		}
	}
}

void GameEngine::RenderGame(void)
{
	int i, count;
	int objTotal = (int) objects->size();
	count = (int)billboards->size();

	for(i = 0; i < count; i++)
	{
		objects->push_back(*billboards->at(i).GetObject3D());
	}

	objects->push_back(*doorA);
	objects->push_back(*doorB);
	objects->push_back(*movingPlat);
	
	count = (int)buttons->size();
	for(i = 0; i < count; i++)
	{
		if(buttons->at(i).RenderObject())
		{
			objects->push_back(*buttons->at(i).GetObject3D());
		}
	}

	count = (int)pickups->size();
	for(i = 0; i < count; i++)
	{
		objects->push_back(*pickups->at(i).GetObject3D());
	}

	objects->push_back(*suit);
	objects->push_back(*rotatingFloor);

	objects->push_back(*player->GetCharacter());
	objects->push_back(transObjects->at(0));

	renderEngine->Render(d3DDevice, player->GetCamera(), objects, lights, directDebug, fog, fogDistance, renderType);

	objTotal = (int)objects->size() - objTotal;
	for(i = 0; i < objTotal; i++)
	{
		objects->pop_back();
	}
}

void GameEngine::UpdateScreen(UINT width, UINT height)
{
	player->GetCamera()->SetScreen(width, height);
}

void GameEngine::ReleaseResources(void)
{
	SafeReleaseDelete(d3DDevice);
	SafeReleaseDelete(inputManager);
	int model;
	int count = (int)objects->size();
	for(model = 0; model < count; model++)
	{
		objects->at(model).ReleaseResources();
	}

	count = (int)transObjects->size();
	for(model = 0; model < count; model++)
	{
		transObjects->at(model).ReleaseResources();
	}

	count = (int)billboards->size();
	for(model = 0; model < count; model++)
	{
		billboards->at(model).GetObject3D()->ReleaseResources();
	}

	count = (int)pickups->size();
	for(model = 0; model < count; model++)
	{
		pickups->at(model).GetObject3D()->ReleaseResources();
	}

	count = (int)buttons->size();
	for(model = 0; model < count; model++)
	{
		buttons->at(model).GetObject3D()->ReleaseResources();
	}

	doorB->ReleaseResources();
	SafeDelete(doorA);
	doorB->ReleaseResources();
	SafeDelete(doorA);
	movingPlat->ReleaseResources();
	SafeDelete(movingPlat);

	SafeDelete(pickups);
	SafeDelete(billboards);
	SafeDelete(collisions);
	SafeDelete(objects);
	SafeDelete(lights);
}