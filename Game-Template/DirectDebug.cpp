#include "stdafx.h"

void DirectDebug::SetupDisplayFont(ID3D11Device *device, ID3D11DeviceContext *deviceContext)
{
	HRESULT hResult = FW1CreateFactory(FW1_VERSION, &debugDFactory);
	hResult = debugDFactory->CreateFontWrapper(device, L"Calibri", &debugDisplay);
}

void DirectDebug::ComputeFrameRate(void)
{
	// A simple mechanism to compute frame rate.
	frames++;
	currentTime = GetTickCount(); 
	DWORD elapsedTime = currentTime - lastUpdate;

	if(elapsedTime >= 1000)
	{
		fps = (frames * 1000.0 / elapsedTime);
		lastUpdate = currentTime;
		frames = 0;
	}
}

void DirectDebug::DebugTextDisplay(Direct3DManager *device, Camera *camera, RenderEngine::RenderType renderType, int polys)
{
	WCHAR debug[1024];	
	const float rightAlign = 5.0f;
	const float lx = camera->XYZAngles().x;
	const float ly = camera->XYZAngles().y;
	const float lz = camera->XYZAngles().z;
	std::wstring render = L"Render Mode: ";

	switch(renderType)
	{
		case RenderEngine::NORMALS:
				render += L"Normals";
			break;
		case RenderEngine::DIFFUSE:
				render += L"Diffuse Texture";
			break;
		case RenderEngine::SPECULAR:
				render += L"Specular";
			break;
		case RenderEngine::SHADOWMAP:
				render += L"Shadow Mapping";
			break;
		case RenderEngine::SOFTSHADOWMAP:
				render += L"Soft Shadow Mapping";
			break;
	}


	debugDisplay->DrawString(device->GetDeviceContext(), L"Graphics 2: Interactive Demo by Bradley Stone & Jack Kemp", 16.0f, 5.0f, 5.0f, 0xffffffff, FW1_RESTORESTATE);

	swprintf_s(debug, L"%s\nFPS: %.2f\nVideo Card: %s\nVideo Card Memory: %dMB\nResolution: %d x %d\n\nCamera: X: %g  Y: %g  Z: %g  -  LookX: %g  LookY: %g\n\nPolygon Count: %d\n\n%s", 
		device->GetDirectXLevel().c_str(), fps,  device->GetVideoCardDescription().c_str(), device->GetVideoCardMemory(), camera->ScreenWidth(), camera->ScreenHeight(),
		camera->Position().x, camera->Position().y, camera->Position().z, lx, ly, polys, render.c_str());
	debugDisplay->DrawString(device->GetDeviceContext(), debug, 14.0f, 5.0f, 40.0f, 0xffffffff, FW1_RESTORESTATE);
		
	swprintf_s(debug, L"Control Scheme:\n\nESC: Pause Game\nMouse: Camera Rotation\nLeft Mouse Click: Object Interaction\nWASD: Movement\nSpace: Jump/Double Jump\n+/-: Change Render Mode\nF12: Fullscreen");
	debugDisplay->DrawString(device->GetDeviceContext(), debug, 14.0f, rightAlign, camera->ScreenHeight() - (camera->ScreenHeight() / 2.15f), 0xffffffff, FW1_RESTORESTATE);
}

void DirectDebug::ReleaseResources(void)
{
	GameEngine::SafeRelease(debugDisplay);
	GameEngine::SafeRelease(debugDFactory);
}