// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

// Windows Header Files:
#include <windows.h>
#include <windowsx.h>
#include <CommDlg.h>

#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <dinput.h>
#include "d3dx10.h"
#include "d3dx11.h"
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Lib\\x86\\dinput8.lib")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Lib\\x86\\dxguid.lib")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Lib\\x86\\dxgi.lib")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Lib\\x86\\d3d11.lib")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Lib\\x86\\d3d11.lib")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Lib\\x86\\d3dx10.lib")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Lib\\x86\\d3dx11.lib")

#include "..\\FW1FontWrapper\\FW1FontWrapper.h"
#pragma comment (lib, "..\\FW1FontWrapper\\x86\\FW1FontWrapper.lib")

// C RunTime Header Files
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>

#include "DirectCollision.h"

#include "Mesh.h"
#include "Object3D.h"
#include "BillboardObject.h"

#include "DirectOrthoWindow.h"
#include "DirectRenderTarget.h"
#include "DirectLight.h"
#include "DirectSpecularShader.h"
#include "DirectTextureShader.h"
#include "DirectShadowShader.h"
#include "DirectNormalShader.h"
#include "DirectDiffuseShader.h"
#include "DirectDepthShader.h"
#include "DirectBlurShader.h"
#include "DirectSShadowShader.h"
#include "DirectRenderTargetManager.h"
#include "DirectInputManager.h"
#include "Camera.h"
#include "DirectShaderManager.h"
#include "Direct3DManager.h"
#include "DirectDebug.h"

#include "ButtonObject.h"
#include "Player.h"
#include "PickupObject.h"

#include "MenuMain.h"
#include "pauseMenu.h"

#include "Utility.h"
#include "RenderEngine.h"
#include "GameEngine.h"