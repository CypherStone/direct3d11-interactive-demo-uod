cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

cbuffer CameraBuffer
{
	float3 camera;
	float2 fogBuffer;
	float3 padding;
};

struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
    float3 binormal : BINORMAL;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
    float4 viewPosition : TEXCOORD1;
	float3 viewDirection : TEXCOORD2;
	float4 worldPosition : TEXCOORD3;
	float3 camPosition : TEXCOORD4;
	float fogFactor : TEXCOORD5;
};

PixelInputType SoftShadowVertexShader(VertexInputType input)
{
	PixelInputType output;    
    input.position.w = 1.0f;

	output.worldPosition = mul(input.position, worldMatrix);

    output.position = output.worldPosition;
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
    output.viewPosition = output.position;
    output.tex = input.tex;
    
    output.normal = mul(input.normal, (float3x3)worldMatrix);
    output.normal = normalize(output.normal);
	output.camPosition = camera.xyz;
	output.viewDirection = output.camPosition.xyz - output.worldPosition.xyz;
	output.viewDirection = normalize(output.viewDirection);

    output.tangent = mul(input.tangent, (float3x3)worldMatrix);
    output.tangent = normalize(output.tangent);

    output.binormal = mul(input.binormal, (float3x3)worldMatrix);
    output.binormal = normalize(output.binormal);

	float4 cam = output.worldPosition;
	cam = mul(cam, viewMatrix);

	output.fogFactor = saturate((fogBuffer.x - cam.z) / (fogBuffer.x - fogBuffer.y));

	return output;
}