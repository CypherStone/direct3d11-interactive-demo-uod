#include "stdafx.h"

Camera::Camera(void)
{
	Init();
}

Camera::~Camera(void)
{
	// Do Nothing
}

void Camera::Initiate(UINT width, UINT height, FLOAT fOV, FLOAT nearView, FLOAT farView, D3DVECTOR position, D3DVECTOR angles)
{
	Init();
	_screenWidth = width;
	_screenHeight = height;
	_fOV = fOV;
	_nearView = nearView;
	_farView = farView;
	_aspectRatio = (FLOAT)width / (FLOAT)height;
	_position = position;
	_angles = angles;
 
    _viewport.TopLeftX = 0;
    _viewport.TopLeftY = 0;
    _viewport.Width = (float)ScreenWidth();
	_viewport.Height = (float)ScreenHeight();
	_viewport.MinDepth = 0.0f;
	_viewport.MaxDepth = 1.0f;

	SetProjection();
}

Camera& Camera::operator=(const Camera &rhs)
{
	if(this != &rhs)
	{
		Copy(rhs);
	}
	return *this;
}

void Camera::Copy(const Camera& cam)
{
	_screenWidth = cam._screenWidth;
	_screenHeight = cam._screenHeight;
	_fOV = cam._fOV;
	_nearView = cam._nearView;
	_farView = cam._farView;
	_aspectRatio = cam._aspectRatio;
	_position = cam._position;
	_angles = cam._angles;
	_view = cam._view;
	_projection =  cam._projection;
	_viewport = cam._viewport;
}

void Camera::Init(void)
{
	ZeroMemory(&_viewport, sizeof(D3D11_VIEWPORT));
	_screenWidth = 0;
	_screenHeight = 0;
	_fOV = 0.0f;
	_nearView = 0.0f;
	_farView = 0.0f;
	_aspectRatio = 0.0f;
	_position = D3DVECTOR();
	_angles = D3DVECTOR();
	_view = D3DXMATRIX();
	_projection = D3DXMATRIX();
	character = D3DXVECTOR3();
}

void Camera::SetProjection(void)
{
	D3DXMatrixPerspectiveFovLH(&_projection, 
	_fOV,									// Field of View
	_aspectRatio,							// Aspect Ratio
	_nearView,								// Near View-plane
	_farView);								// Far View-plane
}

void Camera::BackwardMovement(float speed)
{
	D3DXVECTOR3 lookAt(0.0f, 0.0f, 1.0f);
	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	yaw   = -_angles.y;
	pitch = 0;
	roll  = 0;

	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	
	_position -= lookAt * speed;
	lookAt = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	
	yaw   = -_angles.y;
	pitch = _angles.x;
	roll  = _angles.z;
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	
	lookAt = _position + lookAt;

	D3DXMatrixLookAtLH(&_view, &_position, &lookAt, &up);
}

void Camera::ForwardMovement(float speed)
{
	D3DXVECTOR3 lookAt(0.0f, 0.0f, 1.0f);
	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	yaw   = -_angles.y;
	pitch = 0;
	roll  = 0;

	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	
	_position += lookAt * speed;
	lookAt = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	
	yaw   = -_angles.y;
	pitch = _angles.x;
	roll  = _angles.z;
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	
	lookAt = _position + lookAt;

	D3DXMatrixLookAtLH(&_view, &_position, &lookAt, &up);
}

void Camera::StrafeLeft(float speed)
{
	D3DXVECTOR3 lookAt(1.0f, 0.0f, .0f);
	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	yaw   = -_angles.y;
	pitch = 0;
	roll  = 0;

	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	
	_position -= lookAt * speed;
	lookAt = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	
	yaw   = -_angles.y;
	pitch = _angles.x;
	roll  = _angles.z;
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	
	lookAt = _position + lookAt;

	D3DXMatrixLookAtLH(&_view, &_position, &lookAt, &up);
}

void Camera::StrafeRight(float speed)
{
	D3DXVECTOR3 lookAt(1.0f, 0.0f, .0f);
	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	yaw   = -_angles.y;
	pitch = 0;
	roll  = 0;

	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	
	_position += lookAt * speed;
	lookAt = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

	yaw   = -_angles.y;
	pitch = _angles.x;
	roll  = _angles.z;
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	
	lookAt = _position + lookAt;

	D3DXMatrixLookAtLH(&_view, &_position, &lookAt, &up);
}

void Camera::InclineHeight(float speed)
{
	_position.y += speed;
}
void Camera::DeclineHeight(float speed)
{
	_position.y -= speed;
}

void Camera::Update(DirectInputManager *inputManager)
{
	_angles.x += (float)inputManager->GetMouseY() / 300.0f;
	_angles.y -= (float)inputManager->GetMouseX() / 300.0f;

	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	D3DXVECTOR3 lookAt(0.0f, 0.0f, 1.0f);
	
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	if(_angles.x < -0.3f) 
		_angles.x = -0.3f;
	else if(_angles.x >0.6f)
		_angles.x = 0.6f;

	pitch = _angles.x;
	yaw   = -_angles.y;
	roll  = _angles.z;

	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	lookAt = _position + lookAt;

	D3DXMatrixLookAtLH(&_view, &_position, &lookAt, &up);
}

void Camera::GenerateBaseView(void)
{
	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	D3DXVECTOR3 lookAt;

	float radiansY = _angles.y;
	float radiansX = _angles.x;
	lookAt.x = sinf(radiansY) + _position.x;
	lookAt.y = sinf(radiansX) + _position.y;
	lookAt.z = cosf(radiansY) + _position.z;

	D3DXMatrixLookAtLH(&baseView, &_position, &lookAt, &up);
}
