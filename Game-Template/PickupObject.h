#pragma once
#include "stdafx.h"

class PickupObject
{
private:
	Object3D *obj;
	D3DXVECTOR3 velocity;
	bool held;
	bool collided;
	bool button;
	bool buttonGrab;
	bool falling;
	D3DXVECTOR3 previous;
	
public:
	PickupObject(void);
	~PickupObject(void);
	PickupObject(const PickupObject &pick);
	void OnCollision(D3DXVECTOR3 temp);
	void Update(std::vector<Object3D> * objs, std::vector<DirectCollision> *collisions, int self);
	void PlayerUpdate(DirectInputManager *input, Player *player);

	inline void PickUp(bool picked)
	{
		held = picked;
	}

	inline bool IsHeld(void){
		return held;
	}

	inline bool HasCollided(void)
	{
		return collided;
	}

	inline Object3D * GetObject3D(void)
	{
		return obj;
	}
};

