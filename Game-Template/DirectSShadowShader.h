#pragma once
#include "stdafx.h"
#define LIGHTCOUNT 3

class DirectSoftShadowShader
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct LightBufferType
	{
		D3DXVECTOR4 lightAmbient[LIGHTCOUNT];
		D3DXVECTOR4 lightDiffuse[LIGHTCOUNT];
		D3DXVECTOR4 lightSpecular[LIGHTCOUNT];
		D3DXVECTOR4 lightOn[LIGHTCOUNT];
		D3DXVECTOR3 lightPosition[LIGHTCOUNT];
		float lightSpecularPower[LIGHTCOUNT];
	};

	struct CameraBuffer
	{
		D3DXVECTOR3 camera;
		D3DXVECTOR2 fogBuffer;
		D3DXVECTOR3 padding;
	};

	struct MaterialBuffer
	{
		D3DXCOLOR ambient;
		D3DXCOLOR diffuse;
		D3DXCOLOR specular;
		D3DXCOLOR fogColor;
		float specularPower;
		float alpha;
		D3DXVECTOR2 padding;
	};

	ID3D11VertexShader* vertexShader;
	ID3D11PixelShader* pixelShader;
	ID3D11InputLayout* layout;
	ID3D11SamplerState* sampleStateWrap;
	ID3D11SamplerState* sampleStateClamp;
	ID3D11Buffer* matrixBuffer;
	ID3D11Buffer* lightBuffer;
	ID3D11Buffer* lightBuffer2;
	ID3D11Buffer* materialBuffer;

	void RenderShader(ID3D11DeviceContext* deviceContext, int vertexCount);

public:
	DirectSoftShadowShader(void);
	DirectSoftShadowShader(const DirectSoftShadowShader&);
	~DirectSoftShadowShader(void);

	bool SetupShader(ID3D11Device* device, char* vShader, char* pShader);
	bool Render(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
						D3DXMATRIX projectionMatrix, Mesh *mesh, std::vector<DirectLight> *lights, D3DXVECTOR3 camera, D3DXVECTOR2 fog, D3DXCOLOR fogColour);

	void ReleaseResources(void);

};