#pragma once
#include "stdafx.h"

class DirectTextureShader
{

private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	ID3D11VertexShader* vertexShader;
	ID3D11PixelShader* pixelShader;
	ID3D11InputLayout* layout;
	ID3D11Buffer* matrixBuffer;
	ID3D11SamplerState* sampleState;

	void Render(ID3D11DeviceContext* deviceContext, int vertexCount);

public:
	DirectTextureShader(void);
	DirectTextureShader(const DirectTextureShader&);
	~DirectTextureShader(void);

	bool SetupShader(ID3D11Device* device, char* vShader, char* pShader);
	bool RenderShader(ID3D11DeviceContext* deviceContext, int vertexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture);
	void ReleaseResources(void);
};
