#include "Stdafx.h"
#include "ButtonObject.h"

ButtonObject::ButtonObject(void)
{
	obj = new Object3D;
	oneShot = false;
	pressed = false;
	mouseFix = false;
	renderObject = true;
}

ButtonObject::~ButtonObject(void)
{
}

ButtonObject::ButtonObject(const ButtonObject &button)
{
	obj = button.obj;
	oneShot = button.oneShot;
	pressed = button.pressed;
	mouseFix = button.mouseFix;
	renderObject = button.renderObject;
	type = button.type;
}

void ButtonObject::Update(DirectInputManager *input, DirectCollision * playerBounds)
{
	if(obj->GetBounds().IsColliding(*playerBounds))
	{
		if(input->LeftMouseButton())
		{
			if(!mouseFix)
			{
				mouseFix = true;

				if(type == ButtonType::OneHit)
				{
					if(!oneShot)
					{
						oneShot = true;
						renderObject = false;
					}
				}
				else
					pressed = !pressed;
			}
		}
		else
		{
			mouseFix = false;
		}
	}
}
