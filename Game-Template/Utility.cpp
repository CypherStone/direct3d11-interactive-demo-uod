#include "Utility.h"
#include "stdafx.h"

bool Utility::FileDialog(HWND &hWnd, char *fileString, char *title)
{
	// The Windows open file dialog.
	OPENFILENAME fileDialog;
	SecureZeroMemory(&fileDialog, sizeof(fileDialog));
	fileDialog.lStructSize= sizeof(fileDialog);
	fileDialog.hwndOwner = hWnd;
	fileDialog.lpstrTitle = title;
	fileDialog.lpstrFilter = "Object File (*.obj)\0*.obj\0";
	fileDialog.lpstrFile = fileString;
	fileDialog.nMaxFile = MAX_PATH;
	fileDialog.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST;
	fileDialog.lpstrDefExt = "obj";

	if(GetOpenFileName(&fileDialog))
	{
		return true;
	}

	return false;
}

void Utility::BuildFileString(const char *fileString, const std::string toReplace, std::string &newString)
{
	newString = fileString;
	int start = newString.find_last_of('\\');
	std::string endFileName = newString.substr(start + 1);
	newString.replace(newString.find(endFileName), endFileName.size(), toReplace);
}

void Utility::ImportOBJ(Object3D &obj, const char *fileString, ID3D11Device  *device, ID3D11DeviceContext *deviceContext)
{
	// An extension of my original OBJ file loader from Graphics 1.
	// It loads all different type of OBJ files with material and textures.
	// By using the BuildFileString() function, it can retrieve other file relative to the OBJ.
	std::ifstream checkFileExists(fileString);

	if(checkFileExists)
	{
		std::ifstream infile(fileString, std::ifstream::in);
		const UINT size = 1024;

		std::vector<Mesh> meshes;
		std::vector<Mesh::DirectMeshMaterial> materials;
		std::vector<Mesh::DirectVertex> tempVertexes;
		std::vector<D3DVECTOR> tempVertexNormals;
		std::vector<D3DVECTOR> tempVertexTextures;
		std::vector <Mesh::DirectVertex> vertexes;

		std::string mtlLocation;
		int materialNo = 0;
		bool normals = false;
		bool textures = false;
		bool activateMeshes = false;

		while (!infile.eof()) 
		{
			char line[size];
			infile.getline(line, size);
			std::istringstream istring(line, std::istringstream::in);
			char type[size];

			D3DVECTOR vector;
			Mesh::DirectVertex vertex;
			SecureZeroMemory(&vertex, sizeof(vertex));

			int v1, v2, v3;
			int vt1, vt2, vt3;
			int vn1, vn2, vn3;
			char disposable;
			
			istring>>type;
			if(type[0] != '\n' && type[0] != '#')
			{
				if(strcmp(type, "mtllib") == 0)
				{
					istring>>mtlLocation;
					BuildFileString(fileString, mtlLocation, mtlLocation);
					GetMeshMaterial(materials, mtlLocation.c_str());
				}
				else if(strcmp(type, "usemtl") == 0)
				{
					if(activateMeshes)
					{
						meshes.push_back(Mesh(&materials.at(materialNo), vertexes, device, deviceContext));
						vertexes.clear();
					}
					else
					{
						activateMeshes = true;
					}	

					istring>>type;
					for(int i = 0; i < (int)materials.size(); i++)
					{
						if(strcmp(type, materials.at(i).name.c_str()) == 0)
						{
							materialNo = i;
							break;
						}
					}
				}
				else if(strcmp(type, "v") == 0)
				{
					istring>>vertex.position.x>>vertex.position.y>>vertex.position.z;
					tempVertexes.push_back(vertex);
				}
				else if(strcmp(type, "vt") == 0)
				{
					textures = true;
					istring>>vector.x>>vector.y;
					tempVertexTextures.push_back(vector);
				}
				else if(strcmp(type, "vn") == 0)
				{
					normals = true;
					istring>>vector.x>>vector.y>>vector.z;
					tempVertexNormals.push_back(vector);
				}
				else if(type[0] == 'f')
				{	
					if(textures && normals)
					{
						istring>>v1>>disposable>>vt1>>disposable>>vn1>>v2>>disposable>>vt2>>disposable>>vn2>>v3>>disposable>>vt3>>disposable>>vn3;
			
						tempVertexes.at(v1 - 1).normal = tempVertexNormals.at(vn1 - 1);
						tempVertexes.at(v2 - 1).normal = tempVertexNormals.at(vn2 - 1);
						tempVertexes.at(v3 - 1).normal = tempVertexNormals.at(vn3 - 1);
						tempVertexes.at(v1 - 1).texture.x = tempVertexTextures.at(vt1 - 1).x;
						tempVertexes.at(v1 - 1).texture.y = tempVertexTextures.at(vt1 - 1).y;
						tempVertexes.at(v2 - 1).texture.x = tempVertexTextures.at(vt2 - 1).x;
						tempVertexes.at(v2 - 1).texture.y = tempVertexTextures.at(vt2 - 1).y;
						tempVertexes.at(v3 - 1).texture.x = tempVertexTextures.at(vt3 - 1).x;
						tempVertexes.at(v3 - 1).texture.y = tempVertexTextures.at(vt3 - 1).y;

						D3DXVECTOR3 tangent, binormal;
						Mesh::CalculateTangentBinormal(tempVertexes.at(v1 - 1), tempVertexes.at(v2 - 1), tempVertexes.at(v3 - 1), tangent, binormal);
						tempVertexes.at(v1 - 1).binormal = binormal;
						tempVertexes.at(v1 - 1).tangent = tangent;
						tempVertexes.at(v2 - 1).binormal = binormal;
						tempVertexes.at(v2 - 1).tangent = tangent;
						tempVertexes.at(v3 - 1).binormal = binormal;
						tempVertexes.at(v3 - 1).tangent = tangent;

					}
					else if(textures && !normals)
					{
						istring>>v1>>disposable>>vt1>>v2>>disposable>>vt2>>v3>>disposable>>vt3;
					}
					else if(!textures && normals)
					{
						istring>>v1>>disposable>>vn1>>v2>>disposable>>vn2>>v3>>disposable>>vn3;
					}
					else if(!textures && !normals)
					{
						istring>>v1>>v2>>v3;
					}

					vertexes.push_back(tempVertexes.at(v1 - 1));
					vertexes.push_back(tempVertexes.at(v2 - 1));
					vertexes.push_back(tempVertexes.at(v3 - 1));
				}
			}
		}

		infile.close();
		
		meshes.push_back(Mesh(&materials.at(materialNo), vertexes, device, deviceContext));
		obj.SetMeshes(meshes);
	}	
}

void Utility::GetMeshMaterial(std::vector<Mesh::DirectMeshMaterial> &materials, const char *fileString)
{
	// An OBJ Material parser, which abides by the rules of an OBJ Material.
	std::ifstream checkFileExists(fileString);
	if(checkFileExists)
	{
		bool inMaterial = false;
		std::ifstream infile(fileString, std::ifstream::in);
		const UINT size = 1024;
		Mesh::DirectMeshMaterial material;
		bool opacity = false;

		while (!infile.eof()) 
		{	
			CHAR line[size];
			infile.getline(line, size);
			std::istringstream istring(line, std::istringstream::in);
			CHAR type[size];

			istring>>type;

			if(type[0] != '\n' && type[0] != '#')
			{
				if(strcmp(type, "newmtl") == 0)
				{
					// Finds a new material.
					// If not in a material, it setups the proceedure of storing it - this is usually the beginning.
					// Else, it has completed a material structure and adds it the to the vector.
					if(!inMaterial)
					{
						inMaterial = true;
						SecureZeroMemory(&material, sizeof(Mesh::DirectMeshMaterial));
						istring>>type;
						material.name = type;
					}
					else if(inMaterial)
					{
						if(material.mapKo.empty())
						{
							BuildFileString(fileString, std::string("maps\\opacity_default.jpg"), material.mapKo);
						}
						materials.push_back(material);
						SecureZeroMemory(&material, sizeof(Mesh::DirectMeshMaterial));
						istring>>type;
						material.name = type;
					}
				}
				else if(strcmp(type, "Ka") == 0)
				{
					istring>>material.Ka.r>>material.Ka.g>>material.Ka.b;
				}
				else if(strcmp(type, "Kd") == 0)
				{
					istring>>material.Kd.r>>material.Kd.g>>material.Kd.b;
				}
				else if(strcmp(type, "Ks") == 0)
				{
					istring>>material.Ks.r>>material.Ks.g>>material.Ks.b;
				}
				else if(strcmp(type, "Ns") == 0)
				{
					istring>>material.Ni;
				}
				else if(type[0] == 'd')
				{
					istring>>material.alpha;
				}
				else if(strcmp(type, "illum") == 0)
				{
					istring>>material.illum;
				}
				else if(strcmp(type, "map_Kd") == 0)
				{
					istring>>material.mapKd;
					BuildFileString(fileString, material.mapKd, material.mapKd);
				}
				else if(strcmp(type, "map_Kn") == 0)
				{
					istring>>material.mapKn;
					BuildFileString(fileString, material.mapKn, material.mapKn);
				}
				else if(strcmp(type, "map_Ks") == 0)
				{
					istring>>material.mapKs;
					BuildFileString(fileString, material.mapKs, material.mapKs);
				}
				else if(strcmp(type, "map_Ko") == 0)
				{
					istring>>material.mapKo;
					BuildFileString(fileString, material.mapKo, material.mapKo);
				}
			}
		}

		if(material.mapKo.empty())
		{
			BuildFileString(fileString, std::string("maps\\opacity_default.jpg"), material.mapKo);
		}

		// Add the final material to the vector of materials.
		materials.push_back(material);
		infile.close();
	}	
}
