#include "stdafx.h"

Object3D::Object3D(void)
{
	Init();
}

Object3D::Object3D(const Object3D& v)
{
	Init();
	Copy(v);
}

Object3D::~Object3D(void) { }

Object3D& Object3D::operator=(const Object3D &rhs)
{
	Copy(rhs);
	return *this;
}

void Object3D::SetWorldTransform(void)
{
	D3DXMATRIX rotationMatrix;
	D3DXMATRIX translationMatrix;
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, _yAngle, _xAngle, _zAngle);
	D3DXMatrixTranslation(&translationMatrix, _position.x, _position.y ,_position.z);
	_worldMatrix = rotationMatrix * translationMatrix;
	bounds.UpdatePosition(_worldMatrix);
}

void Object3D::Copy(const Object3D& v)
{
	_polycount = v._polycount;
	_xAngle = v._xAngle;
	_yAngle = v._yAngle;
	_zAngle = v._zAngle;
	_position = v._position;
	_meshes = v._meshes;
	_worldMatrix = v._worldMatrix;
	bounds = v.bounds;
}

void Object3D::Init(void)
{
	_polycount = 0;
	_xAngle = 0;
	_yAngle = 0;
	_zAngle = 0;
	_position = D3DXVECTOR3();
	D3DXMatrixIdentity(&_worldMatrix);
}

void Object3D::ReleaseResources(void)
{
	for(int i = 0; i < (int)_meshes.size(); i++)
	{
		_meshes[i].ReleaseResources();
	}
}