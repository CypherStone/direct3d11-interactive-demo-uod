#pragma once
#include "stdafx.h"

class DirectCollision
{

private:
	struct AABoundingBox
	{
		D3DXMATRIX position;
		D3DXVECTOR3 min;
		D3DXVECTOR3 max;
	};

	AABoundingBox bounds;
	std::vector<D3DXVECTOR3> boundBox;
public:
	DirectCollision(void);
	~DirectCollision(void);
	DirectCollision(const DirectCollision& v);
	void CalculateBounds(std::vector<class Mesh> * mesh, D3DXMATRIX world);
	DirectCollision& operator=(const DirectCollision &rhs);
	bool IsColliding(DirectCollision other);
	bool IsTopColliding(DirectCollision other, float offset);
	bool IsBottomColliding(DirectCollision other, float offset);
	bool IsSideColliding(DirectCollision other);
	bool IsInside(DirectCollision other);

	inline void SetMin(D3DXVECTOR3 min){
		bounds.min = min;
	}
	
	inline void SetMax(D3DXVECTOR3 max){
		bounds.max = max;
	}

	inline D3DXVECTOR3 GetMax(void){
		return bounds.max;
	}

	inline D3DXVECTOR3 GetMin(void){
		return bounds.min;
	}

	inline void UpdatePosition(D3DXMATRIX position)
	{
		bounds.position = position;
		D3DXVECTOR4 tempMin;
		D3DXVECTOR4 tempMax;
		D3DXVECTOR3 minVertex = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
		D3DXVECTOR3 maxVertex = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

		for(int i = 0; i < 8; i++)
		{		
			D3DXVECTOR4 pos;
			D3DXVECTOR3 vTemp = boundBox.at(i);
			D3DXVec3Transform(&pos, &vTemp, &position);

			minVertex.x = min(minVertex.x, pos.x);
			minVertex.y = min(minVertex.y, pos.y);
			minVertex.z = min(minVertex.z, pos.z);

			maxVertex.x = max(maxVertex.x, pos.x);
			maxVertex.y = max(maxVertex.y, pos.y);
			maxVertex.z = max(maxVertex.z, pos.z);
		}

		bounds.min = minVertex;
		bounds.max = maxVertex;
	}
};

