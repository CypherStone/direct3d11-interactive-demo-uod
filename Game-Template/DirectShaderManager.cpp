#include "stdafx.h"

DirectShaderManager::DirectShaderManager(void)
{
	depthShader = 0;
	shadowShader = 0;
	blurShader = 0;
	softShadowShader = 0;
	textureShader = 0;
	diffuseShader = 0;
	normalShader = 0;
	specularShader = 0;

	depthShader = new DirectDepthShader;
	shadowShader = new DirectShadowShader;
	blurShader = new DirectBlurShader;
	softShadowShader = new DirectSoftShadowShader;
	textureShader = new DirectTextureShader;
	diffuseShader = new DirectDiffuseShader;
	normalShader = new DirectNormalShader;
	specularShader = new DirectSpecularShader;
}

DirectShaderManager::DirectShaderManager(const DirectShaderManager &shaders) { }

DirectShaderManager::~DirectShaderManager(void) { }

bool DirectShaderManager::SetupShaders(ID3D11Device* device, char *vShader, char* pShader)
{
	vertexShader = vShader;
	pixelShader = pShader;

	if(!depthShader->SetupShader(device, vertexShader, pixelShader))	return false;
	
	if(!shadowShader->SetupShader(device, vertexShader, pixelShader)) return false;

	if(!blurShader->SetupShader(device, vertexShader, pixelShader)) return false;

	if(!softShadowShader->SetupShader(device, vertexShader, pixelShader)) return false;

	if(!textureShader->SetupShader(device, vertexShader, pixelShader)) return false;

	if(!diffuseShader->SetupShader(device, vertexShader, pixelShader)) return false;

	if(!normalShader->SetupShader(device, vertexShader, pixelShader)) return false;

	if(!specularShader->SetupShader(device, vertexShader, pixelShader)) return false;
	
	return true;
}

bool DirectShaderManager::DiffuseRender(ID3D11DeviceContext *deviceContext, Object3D *obj, Camera *camera)
{
	for(int mesh = 0; mesh < (int)obj->GetMeshSize(); mesh++)
	{
		obj->GetMesh(mesh).Render(deviceContext);
		if(!diffuseShader->Render(deviceContext, obj->GetWorldTransform(), camera->ViewMatrix(), camera->ProjectionMatrix(), &obj->GetMesh(mesh)))
			return false;
	}

	return true;
}

bool DirectShaderManager::SpecularRender(ID3D11DeviceContext *deviceContext, Object3D *obj, Camera *camera)
{
	for(int mesh = 0; mesh < (int)obj->GetMeshSize(); mesh++)
	{
		obj->GetMesh(mesh).Render(deviceContext);
		if(!specularShader->Render(deviceContext, obj->GetWorldTransform(), camera->ViewMatrix(), camera->ProjectionMatrix(), &obj->GetMesh(mesh)))
			return false;
	}

	return true;
}

bool DirectShaderManager::NormalRender(ID3D11DeviceContext *deviceContext, Object3D *obj, Camera *camera)
{
	for(int mesh = 0; mesh < (int)obj->GetMeshSize(); mesh++)
	{
		obj->GetMesh(mesh).Render(deviceContext);
		if(!normalShader->Render(deviceContext, obj->GetWorldTransform(), camera->ViewMatrix(), camera->ProjectionMatrix(), &obj->GetMesh(mesh)))
			return false;
	}

	return true;
}

bool DirectShaderManager::DepthRender(ID3D11DeviceContext *deviceContext, Object3D *obj, Camera *camera, DirectLight *light)
{
	for(int mesh = 0; mesh < (int)obj->GetMeshSize(); mesh++)
	{
		obj->GetMesh(mesh).Render(deviceContext);
		if(!depthShader->Render(deviceContext, obj->GetWorldTransform(), light->GetViewMatrix(), light->GetProjectionMatrix(), obj->GetMesh(mesh).PolygonCount()))
			return false;
	}

	return true;
}

bool DirectShaderManager::TextureRender(ID3D11DeviceContext* deviceContext, D3DXMATRIX world, DirectOrthoWindow *window, Camera *camera, DirectRenderTarget *thisTarget, DirectRenderTarget *target)
{
	window->Render(deviceContext);
	if(!textureShader->RenderShader(deviceContext, window->GetIndexCount(), world, camera->GetBaseView(), thisTarget->GetOrthoMatrix(), target->GetShaderResourceView()))
		return false;

	return true;
}

bool DirectShaderManager::BlurRender(ID3D11DeviceContext* deviceContext, D3DXMATRIX world, Camera *camera, DirectRenderTarget *thisTarget, DirectRenderTarget *target, DirectOrthoWindow *window, bool vertical, float screenSize)
{
	window->Render(deviceContext);

	if(!blurShader->Render(deviceContext, window->GetIndexCount(), world, camera->GetBaseView(), thisTarget->GetOrthoMatrix(), target->GetShaderResourceView(), vertical, screenSize))
		return false;

	return true;
}

bool DirectShaderManager::ShadowRender(ID3D11DeviceContext * deviceContext, Object3D *obj, Camera *camera, DirectLight *light, DirectRenderTarget *target)
{
	for(int mesh = 0; mesh < (int)obj->GetMeshSize(); mesh++)
	{
		obj->GetMesh(mesh).Render(deviceContext);
		if(!shadowShader->Render(deviceContext, obj->GetMesh(mesh).PolygonCount(), obj->GetWorldTransform(), 
			camera->ViewMatrix(), camera->ProjectionMatrix(), light->GetViewMatrix(), light->GetProjectionMatrix(), 
			target->GetShaderResourceView(), light->GetPosition(), light->GetAmbient()))
			return false;
	}

	return true;
}

bool DirectShaderManager::SoftShadowRender(ID3D11DeviceContext * deviceContext, Object3D *obj, Camera *camera, std::vector<DirectLight> *lights, D3DXCOLOR fog, D3DXVECTOR2 fogDist)
{
	for(int mesh = 0; mesh < (int)obj->GetMeshSize(); mesh++)
	{
		obj->GetMesh(mesh).Render(deviceContext);
		if(!softShadowShader->Render(deviceContext, obj->GetWorldTransform(), camera->ViewMatrix(), camera->ProjectionMatrix(), &obj->GetMesh(mesh), lights, camera->Position(), fogDist, fog))
			return false;
	}

	return true;
}

void DirectShaderManager::ReleaseResources(void)
{
	GameEngine::SafeReleaseDelete(depthShader);
	GameEngine::SafeReleaseDelete(shadowShader);
	GameEngine::SafeReleaseDelete(blurShader);
	GameEngine::SafeReleaseDelete(textureShader);
	GameEngine::SafeReleaseDelete(softShadowShader);
	GameEngine::SafeReleaseDelete(diffuseShader);
	GameEngine::SafeReleaseDelete(normalShader);
	GameEngine::SafeReleaseDelete(specularShader);
}

