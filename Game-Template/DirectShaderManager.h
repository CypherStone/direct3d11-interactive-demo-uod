#pragma once
#include "stdafx.h"

class DirectShaderManager
{
private:
	DirectDepthShader *depthShader;
	DirectShadowShader *shadowShader;
	DirectBlurShader *blurShader;
	DirectSoftShadowShader *softShadowShader;
	DirectTextureShader *textureShader;
	DirectDiffuseShader *diffuseShader;
	DirectNormalShader *normalShader;
	DirectSpecularShader *specularShader;
	char* vertexShader;
	char* pixelShader;
	
public:
	DirectShaderManager(void);
	DirectShaderManager(const DirectShaderManager &shaders);
	~DirectShaderManager(void);

	bool SetupShaders(ID3D11Device* device, char* vShader, char* pShader);
	void ReleaseResources(void);

	bool DepthRender(ID3D11DeviceContext * deviceContext, Object3D *obj, Camera *camera, DirectLight *light);
	bool DiffuseRender(ID3D11DeviceContext * deviceContext, Object3D *obj, Camera *camera);
	bool NormalRender(ID3D11DeviceContext * deviceContext, Object3D *obj, Camera *camera);
	bool SpecularRender(ID3D11DeviceContext * deviceContext, Object3D *obj, Camera *camera);
	bool TextureRender(ID3D11DeviceContext* deviceContext, D3DXMATRIX world, DirectOrthoWindow *window, Camera *camera, DirectRenderTarget *thisTarget, DirectRenderTarget *target);
	bool BlurRender(ID3D11DeviceContext* deviceContext, D3DXMATRIX world, Camera *camera, DirectRenderTarget *thisTarget, DirectRenderTarget *target, DirectOrthoWindow *window, bool vertical, float screenSize);
	bool ShadowRender(ID3D11DeviceContext * deviceContext, Object3D *obj, Camera *camera, DirectLight *light, DirectRenderTarget *target);
	bool SoftShadowRender(ID3D11DeviceContext * deviceContext, Object3D *obj, Camera *camera, std::vector<DirectLight> *lights, D3DXCOLOR fog, D3DXVECTOR2 fogDist);
};