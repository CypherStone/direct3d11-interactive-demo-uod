#include "stdafx.h"
#include "Player.h"


Player::Player(void)
{
	isMovingF = false;
	isMovingB = false;
	isMovingL = false;
	isMovingR = false;
	isJumping = false;
	isDoubleJump = false;
	spacePressed = false;
	holdingObj = false;
	pickupColided = false;
	character = 0;
	camera = 0;
}

Player::~Player(void) { }

void Player::SetupPlayer(void)
{
	character = new Object3D;
	camera = new Camera;
	velocity = D3DXVECTOR3(0, 0, 0);
	forwardVelocity = 0.0f;
	strafeVelocity = 0.0f;
}

void Player::MoveForward(bool forward)
{
	if(forward && !isMovingB)
	{
		if(forwardVelocity < 4.0f)
			forwardVelocity += 0.25f;

		isMovingF = true;
		camera->ForwardMovement(forwardVelocity);
	}
	else if(forwardVelocity > 0.0f)
	{
		isMovingF = false;
		forwardVelocity -= 0.5f;
		camera->ForwardMovement(forwardVelocity);
	}
}

void Player::MoveBackward(bool backward)
{
	if(backward && !isMovingF)
	{
		if(forwardVelocity > -4.0f)
			forwardVelocity -= 0.25f;

		isMovingB = true;
		camera->ForwardMovement(forwardVelocity);
	}
	else if(forwardVelocity < 0.0f)
	{
		isMovingB = false;
		forwardVelocity += 0.5f;
		camera->ForwardMovement(forwardVelocity);
	}
}

void Player::MoveStrafeLeft(bool left)
{
	if(left && !isMovingR)
	{
		if(strafeVelocity < 4.0f)
			strafeVelocity += 0.25f;

		isMovingL = true;
		camera->StrafeLeft(strafeVelocity);
	}
	else if(strafeVelocity > 0.0f)
	{
		isMovingL = false;
		strafeVelocity -= 0.5f;
		camera->StrafeLeft(strafeVelocity);
	}
}

void Player::MoveStrafeRight(bool right)
{
	if(right && !isMovingL)
	{
		if(strafeVelocity > -4.0f)
			strafeVelocity -= 0.25f;

		isMovingR = true;
		camera->StrafeLeft(strafeVelocity);
	}
	else if(strafeVelocity < 0.0f)
	{
		isMovingR = false;
		strafeVelocity += 0.5f;
		camera->StrafeLeft(strafeVelocity);
	}
}

void Player::Jump(bool jumping)
{
	if(jumping)
	{
		if(!spacePressed)
		{
			spacePressed = true;
			if(!isJumping)
			{
				isJumping = true;
				velocity.y = 5.0f;
			}
			else if(!isDoubleJump)
			{
				isDoubleJump = true;
				velocity.y = 5.0f;
			}
		}
	}
	else
	{
		spacePressed = false;
	}
}

void Player::OnCollision(D3DXVECTOR3 camTemp, D3DXVECTOR3 objTemp, D3DXVECTOR3 temp)
{
	camera->XPosition(camTemp.x);
	camera->ZPosition(camTemp.z);
	camera->YAngle(temp.y);
	character->SetPosition(camera->CharacterPosition());
}

void Player::Update(DirectInputManager * input, std::vector<Object3D> * objs, std::vector<DirectCollision> *collisions)
{
	D3DXVECTOR3 tempPos = camera->Position();
	D3DXVECTOR3 temp = character->GetPosition();
	D3DXVECTOR3 tempAngle = camera->XYZAngles();
	int count = (int)objs->size();
	int i;

	float camY = camera->XYZAngles().y;

	if(character->GetBounds().GetMin().y > objs->at(0).GetBounds().GetMin().y + 17.5f)
	{
		float charHeight = character->GetBounds().GetMax().y - character->GetBounds().GetMin().y;
		charHeight = (charHeight < 0) ? -charHeight : charHeight;

		bool aboveObj = false;

		for(i = 1; i < count && !aboveObj; i++)
		{
			if(character->GetBounds().IsTopColliding(objs->at(i).GetBounds(), 20.0f) && velocity.y < 0)
			{
				velocity.y = 0;
				isJumping = false;
				isDoubleJump = false;
				spacePressed = false;
				aboveObj = true;
				camera->YPosition(objs->at(i).GetBounds().GetMax().y + charHeight + 15.0f);
			}
			else if(character->GetBounds().IsBottomColliding(objs->at(i).GetBounds(), 5.0f) && velocity.y > 0)
			{
				velocity.y = -velocity.y;
				aboveObj = true;
			}
		}

		count = (int)collisions->size();

		for(i = 0; i < count && !aboveObj; i++)
		{
			if(character->GetBounds().IsTopColliding(collisions->at(i), 15.0f) && velocity.y < 0)
			{
				velocity.y = 0;
				isJumping = false;
				isDoubleJump = false;
				spacePressed = false;
				aboveObj = true;
				camera->YPosition(collisions->at(i).GetMax().y + charHeight + 15.0f);
			}
			else if(character->GetBounds().IsBottomColliding(collisions->at(i), 5.0f) && velocity.y > 0)
			{
				velocity.y = -velocity.y;
				aboveObj = true;
			}
		}

		if(!aboveObj)
		{
			float gravity = -0.25f;
			velocity.y = (velocity.y + gravity < -7.25) ? -7.25 : velocity.y += gravity;
		}
	}
	else
	{
		velocity.y = 0;
		isJumping = false;
		isDoubleJump = false;
		spacePressed = false;
	}

	camera->Update(input);

	MoveForward(input->IsKeyDown(DIK_W));
	MoveBackward(input->IsKeyDown(DIK_S));
	MoveStrafeLeft(input->IsKeyDown(DIK_A));
	MoveStrafeRight(input->IsKeyDown(DIK_D));
	Jump(input->IsKeyDown(DIK_SPACE));

	camera->PositionVelocity(velocity);
	character->SetPosition(camera->CharacterPosition());
	character->YAngle(-camera->XYZAngles().y);

	if(!character->GetBounds().IsInside(objs->at(0).GetBounds()))
	{
		OnCollision(tempPos, temp, tempAngle);
	}
	else
	{
		count = (int)objs->size();
		for(i = 1; i < count; i++)
		{
			if(character->GetBounds().IsColliding(objs->at(i).GetBounds()))
			{
				OnCollision(tempPos, temp, tempAngle);
			}
		}

		count = (int)collisions->size();

		for(i = 0; i < count; i++)
		{
			if(character->GetBounds().IsColliding(collisions->at(i)))
			{
				OnCollision(tempPos, temp, tempAngle);
			}
		}
	}

	if(pickupColided)
	{
		velocity.y = (velocity.y > 0) ? -velocity.y : velocity.y;
		camera->XPosition(pickupPrevious.x);
		camera->ZPosition(pickupPrevious.z);
		camera->PositionVelocity(velocity);
		camera->YAngle(pickupPreviousY);
		camera->BackwardMovement(2.5f);
		character->SetPosition(camera->CharacterPosition());
	}
}