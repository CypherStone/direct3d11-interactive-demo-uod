#pragma once
#include "stdafx.h"

class RenderEngine
{
private:
	DirectRenderTargetManager *targetManager;
	DirectShaderManager *shaderManager;	
	DirectOrthoWindow *smallWindow;
	DirectOrthoWindow *fullWindow;
	bool renderDebug;
	int shadowMapWidth;
	int shadowMapHeight;

	void Initialize(void);
	void RenderDepth(Direct3DManager *d3DManager, Camera *camera,  std::vector<Object3D> *objects, DirectLight *lights);
	void RenderShadow(Direct3DManager *d3DManager, Camera *camera,  std::vector<Object3D> *objects, DirectLight *lights);
	void RenderDownSample(Direct3DManager *d3DManager, Camera *camera, DirectRenderTarget *target);
	void RenderShadowBlur(Direct3DManager *d3DManager, Camera *camera, DirectRenderTarget *target);
	void RenderUpSample(Direct3DManager *d3DManager, Camera *camera, DirectRenderTarget *target);
	void RenderNormals(Direct3DManager *d3DManager, Camera *camera, Object3D *objects);
	void RenderSpecular(Direct3DManager *d3DManager, Camera *camera, Object3D *objects);

	bool FogCulling(Object3D * obj, Camera * cam, float distance);

public:
	enum RenderType
	{
		NORMALS,
		DIFFUSE,
		SPECULAR,
		SHADOWMAP,
		SOFTSHADOWMAP
	};

	RenderEngine(void);
	RenderEngine(const RenderEngine &engine);
	~RenderEngine(void);

	bool SetupEngine(Direct3DManager *d3DManager, int shadowHeight, int shadowWidth);
	void Render(Direct3DManager *d3DManager, Camera *camera, std::vector<Object3D> *objects, std::vector<DirectLight> *lights, class DirectDebug *directDebug, D3DXCOLOR fog, D3DXVECTOR2 fogDist, RenderType renderType);
	void ReleaseResources(void);
	inline void RenderDebug(bool text){
		renderDebug = text;
	}
};