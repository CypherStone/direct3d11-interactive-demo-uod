#pragma once
#include "stdafx.h"

class BillboardObject
{
private:
	Object3D *obj;
	D3DXVECTOR3 position;

public:
	BillboardObject(void);
	BillboardObject(const BillboardObject &billboard);
	~BillboardObject(void);

	void Update(D3DXVECTOR3 camera);
	inline BillboardObject& operator=(const BillboardObject &rhs){
		position = rhs.position;
		obj = rhs.obj;
	}

	inline Object3D *GetObject3D(void){
		return obj;
	}

	inline D3DXVECTOR3 &GetPosition(void){
		return position;
	}
};

