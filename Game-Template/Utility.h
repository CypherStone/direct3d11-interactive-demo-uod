#pragma once
#include "stdafx.h"

class Utility
{
private:
		IFW1Factory *debugDFactory;
		IFW1FontWrapper *debugDisplay;

public:
	static bool FileDialog(HWND &hWnd, char *fileString, char *title);
	static void BuildFileString(const char *fileString, const std::string toReplace, std::string &newString);
	static void ImportOBJ(Object3D &obj, const char *fileString, ID3D11Device  *device, ID3D11DeviceContext *deviceContext);
	static void GetMeshMaterial(std::vector<Mesh::DirectMeshMaterial> &materials, const char *fileString);
};