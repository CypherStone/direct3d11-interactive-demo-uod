#include "stdafx.h"



InputControl::InputControl(void)
{
	Init();
}

InputControl::InputControl(const InputControl &input)
{
	Init();
	Copy(input);
}

InputControl::~InputControl(void)
{

}

void InputControl::InputUpdate(Camera &camera)
{
	if(_controls == NOINPUT)
		return;

	switch(_controls)
	{
		case MOUSE:
			{
				camera.XAngle(_mouseX);
				camera.YAngle(_mouseY);
			}
			break;
		case FORWARD:
			{
				camera.XPosition(camera.Position().x + (cosf(camera.XYZAngles().x) *  0.1f));
				//camera.YAngle(_mouseY);
			}
			break;
	}
}

void InputControl::GetInput(const HWND &hWnd, const UINT &msg, const WPARAM &wParam, const LPARAM &lParam)
{
	switch(msg)
	{
		case WM_MOUSEWHEEL:
			{
			}
			break;
		case WM_KEYDOWN:
			{
				if ( wParam == 'W' )
				{
					_controls = FORWARD;
				}
			}
			break;
		case WM_MOUSEMOVE:
			{
				_controls = MOUSE;
				POINT mouse;
				GetCursorPos(&mouse);

				_mouseX = mouse.x * _movement;
				_mouseY = mouse.y * _movement;
			}
			break;
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_LBUTTONDBLCLK:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_RBUTTONDBLCLK:

		case WM_KEYUP:
			{
				_controls = NOINPUT;
			}
			break;

	}
}

void InputControl::Init(void)
{
	_controls = NOINPUT;
	_mouseX = 0.0f;
	_mouseY = 0.0f;
	_movement = 0.01f;
}

void InputControl::Copy(const InputControl &input)
{
	_controls = input._controls;
	_mouseX = input._mouseX;
	_mouseY = input._mouseY;
	_movement = input._movement;
}