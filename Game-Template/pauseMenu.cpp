#include "stdafx.h"

pauseMenu::pauseMenu(void) { }

pauseMenu::~pauseMenu(void) { }

void pauseMenu::InitMenu(ID3D11Device *devices)
{
	state = Play;
	HRESULT hResult = FW1CreateFactory(FW1_VERSION, &menuFactory);
	hResult = menuFactory->CreateFontWrapper(devices, L"Impact", &menuDisplay);
	buttonPressed = false;
}

pauseMenu::ReturnStates pauseMenu::UpdateMenu(DirectInputManager *inputManager)
{
	// if W pushed state changes
	if(inputManager->Update())
	{
		if(inputManager->IsKeyDown(DIK_W) || inputManager->IsKeyDown(DIK_S) || inputManager->IsKeyDown(DIK_UP) || inputManager->IsKeyDown(DIK_DOWN))
		{
			if(!buttonPressed)
			{
				buttonPressed = true;
				if (state == Play)	
					state = Exit;
				else				
					state = Play;
			}
		}
		else
		{
			buttonPressed = false;
		}

		// if enter pushed return true
		if (inputManager->IsKeyDown(DIK_RETURN))
		{
			if (state == Play)	
				return ReturnStates::Game;
			else				
				return ReturnStates::ExitGame;
		}
	}

	return ReturnStates::Menu;
}

void pauseMenu::Render(Direct3DManager *device)
{
	WCHAR menu[1024];
	swprintf_s(menu, L"PAUSED");
	menuDisplay->DrawString(device->GetDeviceContext(), menu, 64.0f, 1280 / 2  - 75, 720 / 2 - 200, 0xff000077, FW1_RESTORESTATE);
		
	if (state == Play)
	{
		swprintf_s(menu, L"PLAY");
		menuDisplay->DrawString(device->GetDeviceContext(), menu, 64.0f, 1280 / 2 - 50, 720 / 2, 0xff0000ff, FW1_RESTORESTATE);
		swprintf_s(menu, L"EXIT");
		menuDisplay->DrawString(device->GetDeviceContext(), menu, 50.0f, 1280 / 2 - 35, 720 / 2 + 75, 0xff000077, FW1_RESTORESTATE);
	}
	else
	{
		swprintf_s(menu, L"PLAY");
		menuDisplay->DrawString(device->GetDeviceContext(), menu, 50.0f, 1280 / 2 - 45, 720 / 2, 0xff000077, FW1_RESTORESTATE);
		swprintf_s(menu, L"EXIT");
		menuDisplay->DrawString(device->GetDeviceContext(), menu, 64.0f, 1280 / 2 - 50, 720 / 2 + 75, 0xff0000ff, FW1_RESTORESTATE);
	}
}

void pauseMenu::Delete()
{
	GameEngine::SafeRelease(menuDisplay);
	GameEngine::SafeRelease(menuFactory);
}