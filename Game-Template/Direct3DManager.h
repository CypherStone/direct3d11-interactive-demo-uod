#pragma once
#include "stdafx.h"

class Direct3DManager
{
private:
	ID3D11Device *device;
	IDXGISwapChain *swapChain;
	ID3D11DeviceContext *deviceContext;
	ID3D11RenderTargetView *renderTarget;
	ID3D11Texture2D* depthBuffer;
	ID3D11DepthStencilState* depthState;
	ID3D11DepthStencilState* depthStateZOff;
	ID3D11BlendState* alphaState;
	ID3D11BlendState* alphaStateOff;
	ID3D11DepthStencilView* depthView;
	ID3D11RasterizerState* rasterState;

	bool fullScreen;
	bool vSync;
	int videoCardMemory;
	std::wstring videoCardDescription;
	std::wstring currentLevel;

public:

	Direct3DManager(void);
	Direct3DManager(const Direct3DManager &handle);
	~Direct3DManager(void);

	bool SetupDirectX11(HWND hWnd, Camera cam, bool vsync, bool fullscreen);

	void BeginRender(D3DXVECTOR4 colour);
	void EndRender(void);

	void EnableZBuffer(void);
	void DisableZBuffer(void);

	void EnableTransparency(void);
	void DisableTransparency(void);

	void ReleaseResources(void);

	inline ID3D11DeviceContext *GetDeviceContext(void){
		return deviceContext;
	}

	void SetFullscreen(void);

	inline ID3D11Device *GetDevice(void){
		return device;
	}

	inline int GetVideoCardMemory(void){
		return videoCardMemory;
	}

	inline std::wstring GetVideoCardDescription(void){
		return videoCardDescription;
	}

	inline std::wstring GetDirectXLevel(void){
		return currentLevel;
	}

	inline void SetBackBuffer(void)	{
		deviceContext->OMSetRenderTargets(1, &renderTarget, depthView);
	}

	inline void SetViewport(Camera *camera)	{
		deviceContext->RSSetViewports(1, &camera->GetViewport());
	}
};