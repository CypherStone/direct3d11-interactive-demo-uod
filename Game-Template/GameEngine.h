#pragma once
#include "stdafx.h"

class GameEngine
{
	private:
		HWND hWnd;
		Direct3DManager *d3DDevice;
		DirectInputManager *inputManager;
		RenderEngine *renderEngine;
		DirectDebug *directDebug;
		Player * player;
		MenuMain menu;
		pauseMenu pause;
		RenderEngine::RenderType renderType;


		float doorAnim;
		float movingPlatpos;
		bool direction;

		bool lightAnimation1;
		bool lightAnimation2;
		bool gameRunning;
		bool changeRenderPlus;
		bool changeRenderMinus;
		bool changeFullscreen;
		bool renderDebugText;
		bool f1Fix;

		Object3D *doorA;
		Object3D *doorB;
		Object3D *movingPlat;
		Object3D *rotatingFloor;
		Object3D *suit;

		float suitAngle;
		float suitXAngle;
		float suitYAngle;

		D3DXCOLOR fog;
		D3DXVECTOR2 fogDistance;
		bool fogChange;

		std::vector<Object3D> *objects;
		std::vector<Object3D> *transObjects;
		std::vector<DirectLight> *lights;
		std::vector<DirectCollision> *collisions;
		std::vector<BillboardObject> *billboards;
		std::vector<ButtonObject> *buttons;
		std::vector<PickupObject> *pickups;

		void Initiate(void);
		void Copy(const GameEngine &game);
		void LoadModels(void);
		void SetupLights(void);

		void UpdateInput(void);
		void UpdateGame(void);
		void RenderGame(void);

		enum States
		{
			Menu,
			Game,
			Pause
		};

		States states;

	public:
		GameEngine(void);
		GameEngine(const GameEngine &game);
		~GameEngine(void);

		bool SetupEngine(HINSTANCE hInstance, HWND hwnd, UINT width, UINT height);
		void GameLoop(void);
		void UpdateScreen(UINT width, UINT height);
		void ReleaseResources(void);

		inline bool IsGameRunning(void)
		{
			return gameRunning;
		}

		template<typename T> static inline void SafeRelease(T*& a)
		{
			if(a)
			{
				a->Release();
				a = 0;
			}
		}
		template<typename T> static inline void SafeReleaseDelete(T*& a)
		{
			if(a)
			{
				a->ReleaseResources();
				delete a;
				a = 0;
			}
		}
		template<typename T> static inline void SafeDelete(T*& a) 
		{
			if(a)
			{
				delete a;
				a = 0;
			}
		}
};