#include "StdAfx.h"


PickupObject::PickupObject(void)
{
	held = false;
	collided = false;
	obj = new Object3D;
	velocity = D3DXVECTOR3();
	button = true;
	buttonGrab = false;
	falling = false;
}

PickupObject::~PickupObject(void)
{

}

PickupObject::PickupObject(const PickupObject &pick)
{
	obj = pick.obj;
	velocity = pick.velocity;
	collided = pick.collided;
	held = pick.held;
	button = pick.button;
	buttonGrab = pick.buttonGrab;
	falling = pick.falling;
}

void PickupObject::OnCollision(D3DXVECTOR3 temp)
{
	collided = true;
	if(!held)
	{
		if(falling)
		{
			temp.y = obj->GetPosition().y;
		}
		obj->SetPosition(temp);
	}
}

void PickupObject::PlayerUpdate(DirectInputManager *input, Player *player)
{
	if(!player->IsHoldingObject())
	{
		DirectCollision bounds = obj->GetBounds();
		D3DXVECTOR3 vertex = bounds.GetMax();
		vertex.x += 5.0f;
		vertex.y += 5.0f;
		vertex.z += 5.0f;
		bounds.SetMax(vertex);
		vertex = bounds.GetMin();
		vertex.x -= 5.0f;
		vertex.y -= 5.0f;
		vertex.z -= 5.0f;
		bounds.SetMin(vertex);

		if(bounds.IsColliding(player->GetCharacter()->GetBounds()))
		{
			if(input->LeftMouseButton())
			{
				if(!buttonGrab)
				{
					buttonGrab = true;
					held = true;
					player->HoldingObject(true);
				}
			}
			else
			{
				buttonGrab = false;
			}
		}
	}
	else
	{
		if(held)
		{
			if(input->LeftMouseButton())
			{
				if(!button)
				{
					button = true;
					held = false;
					player->HoldingObject(false);
					player->PickUpCollided(false);
				}
			}
			else
			{
				button = false;
			}
		}
	}

	if(held)
	{
		D3DXVECTOR3 pos = player->GetCamera()->ObjectOffset(D3DXVECTOR3(0.0f, -55.0f, 150.0f));
		obj->SetPosition(pos);
		obj->YAngle(-player->GetCamera()->XYZAngles().y);
		player->PickUpCollided(collided);
		player->PreviousPickupCam(player->GetCamera()->Position());
		player->PreviousPickupCamY(player->GetCamera()->XYZAngles().y);
	}
}

void PickupObject::Update(std::vector<Object3D> * objs, std::vector<DirectCollision> *collisions, int self)
{
	falling = false;
	collided = false;
	previous = obj->GetPosition();
	int count = (int)objs->size();
	int i;

	if(obj->GetBounds().GetMin().y > objs->at(0).GetBounds().GetMin().y + 20.0f)
	{
		float charHeight = obj->GetBounds().GetMax().y - obj->GetBounds().GetMin().y;
		charHeight = (charHeight < 0) ? -charHeight : charHeight;

		bool aboveObj = false;

		for(i = 1; i < count && !aboveObj; i++)
		{
			if(obj->GetBounds().IsTopColliding(objs->at(i).GetBounds(), 5.0f) && velocity.y < 0)
			{
				velocity.y = 0;
				aboveObj = true;
				falling = false;
				previous.y = objs->at(i).GetBounds().GetMax().y + charHeight - 30.0f;
			}
			else if(obj->GetBounds().IsBottomColliding(objs->at(i).GetBounds(), 5.0f) && velocity.y > 0)
			{
				velocity.y = -velocity.y;
				aboveObj = true;
			}
		}

		count = (int)collisions->size();

		for(i = 0; i < count && !aboveObj; i++)
		{
			if(i != self)
			{
				if(obj->GetBounds().IsTopColliding(collisions->at(i), 5.0f) && velocity.y < 0)
				{
					velocity.y = 0;
					aboveObj = true;
					falling = false;
					previous.y = collisions->at(i).GetMax().y + charHeight - 30.0f;
				}
				else if(obj->GetBounds().IsBottomColliding(collisions->at(i), 5.0f) && velocity.y > 0)
				{
					velocity.y = -velocity.y;
					aboveObj = true;
				}
			}
		}

		if(!aboveObj)
		{
			if(!held)
			{
				float gravity = -0.25f;
				velocity.y = (velocity.y + gravity < -7.25) ? -7.25 : velocity.y += gravity;
				falling = true;
			}
		}
	}
	else
	{
		velocity.y = 0;
	}

	if(!held && falling)
	{
		obj->SetPositionVelocity(velocity);
	}

	if(!obj->GetBounds().IsInside(objs->at(0).GetBounds()))
	{
		OnCollision(previous);
	}
	else
	{
		count = (int)objs->size();
		for(i = 1; i < count; i++)
		{
			if(obj->GetBounds().IsColliding(objs->at(i).GetBounds()))
			{
				OnCollision(previous);
			}
		}

		count = (int)collisions->size();

		for(i = 0; i < count; i++)
		{
			if(i != self)
			{
				if(obj->GetBounds().IsColliding(collisions->at(i)))
				{	
					OnCollision(previous);
				}
			}
		}
	}
}