#pragma once
#include "stdafx.h"

class pauseMenu
{

public:
	pauseMenu(void);
	~pauseMenu(void);

	enum ReturnStates{
		Menu,
		Game,
		ExitGame
	};

	void InitMenu(ID3D11Device *devices);
	ReturnStates UpdateMenu(DirectInputManager *inputManager);
	void Render(Direct3DManager *device);
	void Delete(void);

private:

	enum States {
		Play,
		Exit
	};

	bool buttonPressed;
	IFW1Factory *menuFactory;
	IFW1FontWrapper *menuDisplay;
	States state;

};