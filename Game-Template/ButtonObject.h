#pragma once
class ButtonObject
{
public:
	enum ButtonType
	{
		OneHit,
		Pushable
	};

private:
	Object3D *obj;
	D3DXVECTOR3 position;
	bool oneShot;
	bool pressed;
	bool mouseFix;
	bool renderObject;
	ButtonType type;

public:

	ButtonObject(void);
	~ButtonObject(void);
	ButtonObject(const ButtonObject &button);

	void Update(DirectInputManager *input, DirectCollision * playerBounds);

	inline bool OneShot(void){
		return oneShot;
	}

	inline bool Pressed(void){
		return pressed;
	}

	inline void SetType(ButtonType bt){
		type = bt;
	}

	inline ButtonObject& operator=(const ButtonObject &rhs){
		position = rhs.position;
		obj = rhs.obj;
	}

	inline bool RenderObject(void){
		return renderObject;
	}

	inline Object3D *GetObject3D(void){
		return obj;
	}

	inline D3DXVECTOR3 &GetPosition(void){
		return position;
	}
};

