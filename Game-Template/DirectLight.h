#pragma once
#include "stdafx.h"

class DirectLight
{
private:
	D3DXVECTOR4 ambient;
	D3DXVECTOR4 diffuse;
	D3DXVECTOR3 position;
	D3DXVECTOR4 specular;
	float specularPower;
	D3DXVECTOR3 lookat;
	D3DXVECTOR3 emissive;
	float lightDistanceSquared;
	D3DXMATRIX view;
	D3DXMATRIX projection;
	bool lightOn;

	int shadowMapHeight;
	int shadowMapWidth;

	DirectRenderTarget *shadowMap;

	void Init(void);
	void Copy(const DirectLight &light);

public:
	DirectLight(void);
	DirectLight(const DirectLight &light);
	~DirectLight(void);

	void SetupLight(D3DXVECTOR4 amb, D3DXVECTOR4 diff, D3DXVECTOR4 spec, float specPow, D3DXVECTOR3 pos, D3DXVECTOR3 emis, float distance);
	bool SetupShadowMap(ID3D11Device *device, int width, int height, D3DXVECTOR2 screen); 

	D3DXVECTOR4 GetAmbient(void);
	D3DXVECTOR4 GetDiffuse(void);
	D3DXVECTOR4 GetSpecular(void);
	float GetSpecularPower(void);
	D3DXVECTOR3 GetPosition(void);
	D3DXVECTOR3 GetEmissive(void);
	float GetLightDistance(void);

	void GenerateProjection(float screenDepth, float screenNear);
	void GenerateView(void);
	void SetAmbient(float red, float green, float blue, float alpha);
	void SetDiffuse(float red, float green, float blue, float alpha);
	void SetSpecular(float red, float green, float blue, float alpha);
	void SetPosition(float x, float y, float z);
	void SetSpecularPower(float power);

	inline bool LightOn(void){
		return lightOn;
	}

	inline void SetLightOn(bool temp){
		lightOn = temp;
	}

	inline DirectRenderTarget *GetShadowMap(void){
		return shadowMap;
	}

	inline D3DXMATRIX GetViewMatrix(void){
		return view;
	}

	inline D3DXMATRIX GetProjectionMatrix(void){
		return projection;
	}

	inline void SetLookat(float x, float y, float z){
		lookat = D3DXVECTOR3(x, y, z);
	}

	inline int GetShadowWidth(void){
		return shadowMapWidth;
	}

	inline int GetShadowHeight(void){
		return shadowMapHeight;
	}
};