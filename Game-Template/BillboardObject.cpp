#include "Stdafx.h"

BillboardObject::BillboardObject(void) 
{ 
	obj = new Object3D;
	position = D3DXVECTOR3();
}

BillboardObject::BillboardObject(const BillboardObject &billboard)
{
	obj = billboard.obj;
	position= billboard.position;
}

BillboardObject::~BillboardObject(void) 
{ 

}

void BillboardObject::Update(D3DXVECTOR3 camera)
{
	double angle = atan2(position.x - camera.x, position.z - camera.z) * (180.0 / D3DX_PI);
	float rotation = (float)angle * 0.0174532925f;
	obj->SetPosition(position);
	obj->YAngle(rotation);
}
