#include "stdafx.h"
#include "DirectCollision.h"

DirectCollision::DirectCollision(void)
{
}


DirectCollision::~DirectCollision(void)
{
}

DirectCollision::DirectCollision(const DirectCollision& v)
{
	bounds = v.bounds;
	boundBox = v.boundBox;
}

DirectCollision& DirectCollision::operator=(const DirectCollision &rhs)
{
	bounds = rhs.bounds;
	boundBox = rhs.boundBox;
	return *this;
}

void DirectCollision::CalculateBounds(std::vector<Mesh> * mesh, D3DXMATRIX world)
{
	D3DXVECTOR3 minVertex = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	D3DXVECTOR3 maxVertex = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	int meshes = (int)mesh->size();
	int i;
	for(int j = 0; j < meshes; j++)
	{
		int verts = (int)mesh->at(j).GetVertices()->size();
		for(i = 0; i < verts; i++)
		{		
			D3DXVECTOR4 pos;
			D3DXVECTOR3 vTemp = mesh->at(j).GetVertices()->at(i).position;
			D3DXVec3Transform(&pos, &vTemp, &world);

			minVertex.x = min(minVertex.x, pos.x);
			minVertex.y = min(minVertex.y, pos.y);
			minVertex.z = min(minVertex.z, pos.z);

			maxVertex.x = max(maxVertex.x, pos.x);
			maxVertex.y = max(maxVertex.y, pos.y);
			maxVertex.z = max(maxVertex.z, pos.z);
		}
	}

	bounds.min = minVertex;
	bounds.max = maxVertex;

	boundBox.push_back(minVertex);
	boundBox.push_back(maxVertex);
	D3DXVECTOR3 temp = minVertex;
	temp.z = maxVertex.z;
	boundBox.push_back(temp);
	temp.y = maxVertex.y;
	boundBox.push_back(temp);
	temp = minVertex;
	temp.x = maxVertex.x;
	boundBox.push_back(temp);
	temp.z = maxVertex.z;
	boundBox.push_back(temp);
	temp = minVertex;
	temp.y = maxVertex.y;
	boundBox.push_back(temp);
	temp = minVertex;
	temp.y = maxVertex.y;
	temp.x = maxVertex.x;
	boundBox.push_back(temp);
}

bool DirectCollision::IsColliding(DirectCollision other)
{
    return !(
		bounds.min.x > other.bounds.max.x || bounds.max.x < other.bounds.min.x ||
        bounds.min.y > other.bounds.max.y || bounds.max.y < other.bounds.min.y ||
        bounds.min.z > other.bounds.max.z || bounds.max.z < other.bounds.min.z);
}

bool DirectCollision::IsTopColliding(DirectCollision other, float offset)
{
	return !(bounds.min.x > other.bounds.max.x || bounds.max.x < other.bounds.min.x ||
        bounds.min.z > other.bounds.max.z || bounds.max.z < other.bounds.min.z) && (
		bounds.min.y > other.bounds.max.y - offset && 
		bounds.min.y < other.bounds.max.y + offset);

	return false;
}

bool DirectCollision::IsBottomColliding(DirectCollision other, float offset)
{

	return !(
		bounds.min.x > other.bounds.max.x || bounds.max.x < other.bounds.min.x ||
        bounds.min.z > other.bounds.max.z || bounds.max.z < other.bounds.min.z) && (
		bounds.max.y < other.bounds.min.y && 
		bounds.max.y > other.bounds.min.y - offset);

	return false;
}

bool DirectCollision::IsSideColliding(DirectCollision other)
{
	return !(
		bounds.min.x > other.bounds.max.x || bounds.max.x < other.bounds.min.x ||
        bounds.min.y > other.bounds.max.y || bounds.max.y < other.bounds.min.y ||
        bounds.min.z > other.bounds.max.z || bounds.max.z < other.bounds.min.z);
}

bool DirectCollision::IsInside(DirectCollision other)
{
    return
        bounds.min.x >= other.bounds.min.x && bounds.max.x <= other.bounds.max.x &&
		bounds.min.y >= other.bounds.min.y && bounds.max.y <= other.bounds.max.y &&
        bounds.min.z >= other.bounds.min.z && bounds.max.z <= other.bounds.max.z;
}
