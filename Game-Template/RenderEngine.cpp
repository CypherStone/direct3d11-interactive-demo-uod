#include "stdafx.h"

void RenderEngine::Initialize(void)
{
	smallWindow = 0;
	fullWindow = 0;
	shaderManager = 0;
	targetManager = 0;
}

RenderEngine::RenderEngine(void)
{
	Initialize();
}

RenderEngine::RenderEngine(const RenderEngine &engine) { }

RenderEngine::~RenderEngine(void) { }

bool RenderEngine::SetupEngine(Direct3DManager *d3DManager, int shadowWidth, int shadowHeight)
{
	shadowMapWidth = shadowWidth;
	shadowMapHeight = shadowHeight;

	// Setting up the render target manager.
	targetManager = new DirectRenderTargetManager;
	if(targetManager)
	{
		if(!targetManager->SetupRenderManager(d3DManager->GetDevice(), shadowMapWidth, shadowMapHeight, 1.0f, 100.0f))
			return false;
	}
	else
		return false;

	smallWindow = new DirectOrthoWindow;
	if(smallWindow)
	{
		if(!smallWindow->SetupWindow(d3DManager->GetDevice(), shadowMapWidth / 2, shadowMapHeight / 2))
			return false;
	}
	else
		return false;

	fullWindow = new DirectOrthoWindow;
	if(fullWindow)
	{
		if(!fullWindow->SetupWindow(d3DManager->GetDevice(), shadowMapWidth, shadowMapHeight))
			return false;
	}
	else
		return false;

	
	// Setting up the shader manager.
	shaderManager = new DirectShaderManager;
	if(shaderManager)
	{
		if(!shaderManager->SetupShaders(d3DManager->GetDevice(), "vs_4_0", "ps_4_0")) 
			return false;
	}
	else 
		return false;

	return true;
}

bool RenderEngine::FogCulling(Object3D * obj, Camera * cam, float distance)
{
	D3DXVECTOR3 campos = (cam->Position() > 0) ? cam->Position() : -cam->Position();

	if(!(obj->GetPosition().x == 0 && obj->GetPosition().y == 0 && obj->GetPosition().z == 0))
	{
		float x = campos.x - obj->GetPosition().x;
		float y = campos.y - obj->GetPosition().y;
		float z = campos.z - obj->GetPosition().z;
		float dist = sqrt(x * x + y * y + z * z);

		if(dist < distance * 1.50)
			return true;
		else
			return false;
	}
	else
		return true;
}

void RenderEngine::Render(Direct3DManager *d3DManager, Camera *camera, std::vector<Object3D> *objects, std::vector<DirectLight> *lights, DirectDebug *directDebug, D3DXCOLOR fog, D3DXVECTOR2 fogDist, RenderType renderType)
{					
	int polyCount = 0;

	switch(renderType)
	{
		case NORMALS:
			{
				d3DManager->SetBackBuffer();
				d3DManager->SetViewport(camera);			
				for(int model = 0; model < (int)objects->size(); model++)
				{
					if(FogCulling(&objects->at(model), camera, fogDist.y))
					{
						polyCount += (objects->at(model).PolygonCount());
						shaderManager->NormalRender(d3DManager->GetDeviceContext(), &objects->at(model), camera);
					}
				}
			}
			break;
		case DIFFUSE:
			{
				d3DManager->SetBackBuffer();
				d3DManager->SetViewport(camera);			
				for(int model = 0; model < (int)objects->size(); model++)
				{
					if(FogCulling(&objects->at(model), camera, fogDist.y))
					{
						polyCount += (objects->at(model).PolygonCount());
						shaderManager->DiffuseRender(d3DManager->GetDeviceContext(), &objects->at(model), camera);
					}
				}
			}
			break;
		case SPECULAR:
			{
				d3DManager->SetBackBuffer();
				d3DManager->SetViewport(camera);			
				for(int model = 0; model < (int)objects->size(); model++)
				{
					if(FogCulling(&objects->at(model), camera, fogDist.y))
					{
						polyCount += (objects->at(model).PolygonCount());
						shaderManager->SpecularRender(d3DManager->GetDeviceContext(), &objects->at(model), camera);
					}
				}
			}
			break;
		case SHADOWMAP:
			{
				for(int light = 0; light < (int)lights->size(); light++)
				{
					RenderDepth(d3DManager, camera, objects, &lights->at(light));
					RenderShadow(d3DManager, camera, objects, &lights->at(light));
				}

				d3DManager->SetBackBuffer();
				d3DManager->SetViewport(camera);		
				for(int model = 0; model < (int)objects->size(); model++)
				{
					if(FogCulling(&objects->at(model), camera, fogDist.y))
					{
						polyCount += (objects->at(model).PolygonCount());
						shaderManager->SoftShadowRender(d3DManager->GetDeviceContext(), &objects->at(model), camera, lights, fog, fogDist);
					}
				}
			}
			break;
		case SOFTSHADOWMAP:
			{	
				for(int light = 0; light < (int)lights->size(); light++)
				{
					RenderDepth(d3DManager, camera, objects, &lights->at(light));
					RenderShadow(d3DManager, camera, objects, &lights->at(light));
					d3DManager->DisableZBuffer();
					RenderDownSample(d3DManager, camera, lights->at(light).GetShadowMap());
					RenderShadowBlur(d3DManager, camera, targetManager->DownSampleRender());
					RenderUpSample(d3DManager, camera, lights->at(light).GetShadowMap());
					d3DManager->EnableZBuffer();
				}

				d3DManager->SetBackBuffer();
				d3DManager->SetViewport(camera);
				d3DManager->EnableTransparency();
				for(int model = 0; model < (int)objects->size(); model++)
				{		
					if(FogCulling(&objects->at(model), camera, fogDist.y))
					{
						polyCount += (objects->at(model).PolygonCount());
						shaderManager->SoftShadowRender(d3DManager->GetDeviceContext(), &objects->at(model), camera, lights, fog, fogDist);
					}
				}
				d3DManager->DisableTransparency();
			}
			break;
	}

	if(renderDebug)
	{
		directDebug->DebugTextDisplay(d3DManager, camera, renderType, (polyCount / 3) / 3);
	}
}

void RenderEngine::RenderDepth(Direct3DManager *d3DManager, Camera *camera,  std::vector<Object3D> *objects, DirectLight *lights)
{
	targetManager->DepthRender()->SetRenderTarget(d3DManager->GetDeviceContext());
	targetManager->DepthRender()->ClearRenderTarget(d3DManager->GetDeviceContext(), D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f));

	for(int model = 0; model < (int)objects->size(); model++)
	{
		shaderManager->DepthRender(d3DManager->GetDeviceContext(), &objects->at(model), camera, lights);
	}
}

void RenderEngine::RenderShadow(Direct3DManager *d3DManager, Camera *camera,  std::vector<Object3D> *objects, DirectLight *lights)
{
	lights->GetShadowMap()->SetRenderTarget(d3DManager->GetDeviceContext());
	lights->GetShadowMap()->ClearRenderTarget(d3DManager->GetDeviceContext(), D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f));

	for(int model = 0; model < (int)objects->size(); model++)
	{
		shaderManager->ShadowRender(d3DManager->GetDeviceContext(), &objects->at(model), camera, lights, targetManager->DepthRender());
	}
}

void RenderEngine::RenderDownSample(Direct3DManager *d3DManager, Camera *camera, DirectRenderTarget *target)
{
	D3DXMATRIX m;
	D3DXMatrixIdentity(&m);
	targetManager->DownSampleRender()->SetRenderTarget(d3DManager->GetDeviceContext());
	targetManager->DownSampleRender()->ClearRenderTarget(d3DManager->GetDeviceContext(), D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f));
	shaderManager->TextureRender(d3DManager->GetDeviceContext(), m, smallWindow, camera, targetManager->DownSampleRender(), target);
}

void RenderEngine::RenderShadowBlur(Direct3DManager *d3DManager, Camera *camera, DirectRenderTarget *target)
{
	D3DXMATRIX m;
	D3DXMatrixIdentity(&m);
	targetManager->HorizontalBlurRender()->SetRenderTarget(d3DManager->GetDeviceContext());
	targetManager->HorizontalBlurRender()->ClearRenderTarget(d3DManager->GetDeviceContext(), D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f));
	shaderManager->BlurRender(d3DManager->GetDeviceContext(), m, camera, targetManager->HorizontalBlurRender(), target, smallWindow, false, (float)(shadowMapWidth / 2));
	targetManager->VerticalBlurRender()->SetRenderTarget(d3DManager->GetDeviceContext());
	targetManager->VerticalBlurRender()->ClearRenderTarget(d3DManager->GetDeviceContext(), D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f));
	shaderManager->BlurRender(d3DManager->GetDeviceContext(), m, camera, targetManager->VerticalBlurRender(), targetManager->HorizontalBlurRender(), smallWindow, true, (float)(shadowMapHeight / 2));
}

void RenderEngine::RenderUpSample(Direct3DManager *d3DManager, Camera *camera, DirectRenderTarget *target)
{
	D3DXMATRIX m;
	D3DXMatrixIdentity(&m);
	target->SetRenderTarget(d3DManager->GetDeviceContext());
	target->ClearRenderTarget(d3DManager->GetDeviceContext(), D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f));
	shaderManager->TextureRender(d3DManager->GetDeviceContext(), m, fullWindow, camera, target, targetManager->VerticalBlurRender());
}

void RenderEngine::ReleaseResources(void)
{
	GameEngine::SafeReleaseDelete(shaderManager);
	GameEngine::SafeReleaseDelete(targetManager);
	GameEngine::SafeReleaseDelete(smallWindow);
	GameEngine::SafeReleaseDelete(fullWindow);
}