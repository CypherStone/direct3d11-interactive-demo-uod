// Tech Demo - Graphics 2
// Programmer: Bradley Stone
//
// Description: 

#include "stdafx.h"
#include "Win32Program.h"

#define MAX_LOADSTRING 100

GameEngine game;
CONST UINT width = 1280;
CONST UINT height = 720;
CONST UINT windowStartX = 0;
CONST UINT windowStartY = 0;

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    HWND hWnd;
    WNDCLASSEX wc;

    SecureZeroMemory(&wc, sizeof(WNDCLASSEX));

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW + 1;
    wc.lpszClassName = "WindowClass";

    RegisterClassEx(&wc);
    hWnd = CreateWindowEx(NULL, "WindowClass", "Graphics 2: Game Demo", WS_OVERLAPPEDWINDOW, windowStartX, windowStartY, width, height, NULL, NULL, hInstance, NULL);
    ShowWindow(hWnd, nCmdShow);
    
	// Initialise the game engine and the required runtime data
	// If SetupEngine() returns true, activate runtime loop.
	if(game.SetupEngine(hInstance, hWnd, width, height))
	{
		// Hide the cursor.
		ShowCursor(FALSE);

		// Start running the main game loop.
		game.GameLoop();

		// GameEngine loop has exited, release allocated resources.
		game.ReleaseResources();
	}
	else 
	{
		// Setup has failed, display a relative error.
		ShowCursor(TRUE);
		MessageBox(hWnd, "Unable to initiate DirectX11.\nThe application will now close.", "DirectX11 - Setup Error", MB_OK);
	}

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
		case WM_SIZE:
			{
				if(game.IsGameRunning())
				{
					RECT windowSize;
					GetClientRect(hWnd, &windowSize);
					game.UpdateScreen(windowSize.right, windowSize.bottom);
				}
			}
			break;
        case WM_DESTROY:
            {
                PostQuitMessage(0);
                return 0;
            } 
			break;
    }

    return DefWindowProc (hWnd, message, wParam, lParam);
}