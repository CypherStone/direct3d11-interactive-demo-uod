#include "stdafx.h"

class DirectOrthoWindow
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};
	
	ID3D11Buffer *vertexBuffer;
	ID3D11Buffer *indexBuffer;
	int vertexCount;

public:
	DirectOrthoWindow(void);
	DirectOrthoWindow(const DirectOrthoWindow& other);
	~DirectOrthoWindow(void);

	int GetIndexCount(void);

	bool SetupWindow(ID3D11Device*, int, int);
	void Render(ID3D11DeviceContext*);
	void ReleaseResources(void);	
};
