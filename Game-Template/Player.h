#pragma once
#include "stdafx.h"

class Player
{

private:
	bool isJumping, isDoubleJump;
	bool spacePressed;
	bool isMovingF, isMovingB, isMovingL, isMovingR;
	bool holdingObj;
	bool pickupColided;

	Object3D * character;

	Camera * camera;

	D3DXVECTOR3 pickupPrevious;
	float pickupPreviousY;
	D3DXVECTOR3 velocity;
	float forwardVelocity;
	float strafeVelocity;

	void MoveForward(bool forward);
	void MoveBackward(bool backward);
	void MoveStrafeLeft(bool left);
	void MoveStrafeRight(bool right);
	void Jump(bool jump);
	void OnCollision(D3DXVECTOR3 camTemp, D3DXVECTOR3 objTemp, D3DXVECTOR3 temp);

public:
	Player(void);
	~Player(void);
	
	void SetupPlayer(void);	
	void Update(DirectInputManager * input, std::vector<Object3D> * objs, std::vector<DirectCollision> *collisions);
	void Release(void);

	inline bool IsHoldingObject(void){
		return holdingObj;
	}

	inline void HoldingObject(bool hold){
		holdingObj = hold;
	}

	inline void PickUpCollided(bool collided){
		pickupColided = collided;
	}

	inline void PreviousPickupCam(D3DXVECTOR3 collided){
		pickupPrevious = collided;
	}

	inline void PreviousPickupCamY(float collided){
		pickupPreviousY = collided;
	}

	inline Object3D * GetCharacter(void)
	{
		return character;
	}

	inline Camera * GetCamera(void)
	{
		return camera;
	}
};

