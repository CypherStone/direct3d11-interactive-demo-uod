#pragma once
#include "stdafx.h"

class Object3D
{
	private:
		DirectCollision bounds;
		std::vector<Mesh> _meshes;
		D3DXVECTOR3 _position;
		float _xAngle;
		float _yAngle;
		float _zAngle;
		int _polycount;
		D3DXMATRIX _worldMatrix;
		void Init();
		void Copy(const Object3D& v);
		void SetWorldTransform(void);

	public:
		Object3D(void);
		Object3D(const Object3D& v);
		~Object3D(void);

		void Object3D::ReleaseResources(void);

		inline int PolygonCount(void){
			return _polycount;
		}
		
		inline Mesh& GetMesh(int mesh){
			return _meshes.at(mesh);
		}
		
		inline int GetMeshSize(void){
			return _meshes.size();
		}

		inline D3DXMATRIX &GetWorldTransform(void){
			return _worldMatrix;
		}

		inline void CopyObject(Object3D obj){
			Copy(obj);
		}

		Object3D& operator= (const Object3D &rhs);

		inline void SetMeshes(std::vector<Mesh> &meshes){
			_meshes = meshes;
			int meshCount = (int)meshes.size();
			for(int i = 0; i < meshCount; i++)
			{
				_polycount += (meshes[i].PolygonCount() * 3);
			}
			bounds.CalculateBounds(&meshes, _worldMatrix);
		}

		inline void XAngle(float angle){
			_xAngle = angle;
			SetWorldTransform();
		}

		inline float XAngle(void){
			return _xAngle;
		}

		inline void YAngle(float angle){
			_yAngle = angle;
			SetWorldTransform();
		}

		inline float YAngle(void){
			return _yAngle;
		}

		inline void ZAngle(float angle){
			_zAngle = angle;
			SetWorldTransform();
		}

		inline float ZAngle(void){
			return _zAngle;
		}

		inline void XAngleAdd(float angle){
			_xAngle += angle;
			SetWorldTransform();
		}

		inline void YAngleAdd(float angle){
			_yAngle += angle;
			SetWorldTransform();
		}

		inline void ZAngleAdd(float angle){
			_zAngle += angle;
			SetWorldTransform();
		}

		inline D3DXVECTOR3 GetPosition(void){
			return _position;
		}	

		inline void SetPosition(D3DXVECTOR3 nPos){
			_position = nPos;
			SetWorldTransform();
		}

		inline void SetPositionVelocity(D3DXVECTOR3 nPos){
			_position += nPos;
			SetWorldTransform();
		}

		inline void SetMatrix(D3DXMATRIX matrix){
			_worldMatrix = matrix;
		}

		inline DirectCollision GetBounds(void){
			return bounds;
		}
};