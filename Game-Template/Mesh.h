#pragma once
#include "stdafx.h"

class Mesh
{
public:	
	struct DirectMeshMaterial
	{
		std::string name;
		D3DXCOLOR Ka;
		D3DXCOLOR Kd;
		D3DXCOLOR Ks;
		float Ni;
		float alpha;
		float illum;
		std::string mapKd, mapKn, mapKs, mapKo; 
	};

	struct DirectVertex 
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;
	};

private:
	std::vector<DirectVertex> vertices;
	ID3D11Buffer *vertexBuffer;
	ID3D11ShaderResourceView *diffuseTexture;
	ID3D11ShaderResourceView *normalTexture;
	ID3D11ShaderResourceView *specularTexture;
	ID3D11ShaderResourceView *opacityTexture;
	DirectMeshMaterial material;
	int _polycount;
	void Init();
	void Copy(const Mesh& v);

	void SetVertexBuffer(std::vector<DirectVertex>& vertices, ID3D11Device *device, ID3D11DeviceContext *deviceContext);
	void SetMaterial(DirectMeshMaterial *material, ID3D11Device *device);
		
public:

	Mesh(void);
	Mesh(DirectMeshMaterial *material, std::vector<DirectVertex>& vertices, ID3D11Device *device, ID3D11DeviceContext *deviceContext);
	Mesh(const Mesh& v);
	~Mesh(void);

	static void CalculateTangentBinormal(DirectVertex vertex1, DirectVertex vertex2, DirectVertex vertex3, D3DXVECTOR3& tangent, D3DXVECTOR3& binormal);
	Mesh& operator= (const Mesh &rhs);
	void Render(ID3D11DeviceContext *deviceContext);
	void ReleaseResources(void);

	inline int PolygonCount(void){
		return _polycount;
	}

	inline DirectMeshMaterial *GetMaterial(void){
		return &material;
	}

	inline  ID3D11Buffer *VertexBuffer(void){
		return vertexBuffer;
	}

	inline ID3D11ShaderResourceView *GetDiffuse(void){
		return diffuseTexture;
	}

	inline ID3D11ShaderResourceView *GetNormal(void){
		return normalTexture;
	}
	
	inline ID3D11ShaderResourceView *GetSpecular(void){
		return specularTexture;
	}

	inline ID3D11ShaderResourceView *GetOpacity(void){
		return opacityTexture;
	}

	inline std::vector<DirectVertex> * GetVertices(void){
		return &vertices;
	}
};