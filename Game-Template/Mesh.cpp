#include "stdafx.h"

Mesh::Mesh(void)
{
	Init();
}

Mesh::Mesh(DirectMeshMaterial *material, std::vector<DirectVertex>& vertices, ID3D11Device  *device, ID3D11DeviceContext *deviceContext)
{
	Init();
	SetVertexBuffer(vertices, device, deviceContext);
	SetMaterial(material, device);
}

Mesh::Mesh(const Mesh& v)
{
	Copy(v);
}

Mesh::~Mesh(void) { }

void Mesh::Render(ID3D11DeviceContext *deviceContext)
{
	unsigned int stride;
	unsigned int offset;
	stride = sizeof(DirectVertex); 
	offset = 0;
    deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Mesh::SetVertexBuffer(std::vector<DirectVertex>& vertice, ID3D11Device *device, ID3D11DeviceContext *deviceContext)
{
	_polycount = vertice.size();
	vertices = vertice;
	D3D11_BUFFER_DESC vertexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData;

    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(DirectVertex) * _polycount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = 0;
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

    vertexData.pSysMem = &vertice[0];
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

    device->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer);
}

void Mesh::SetMaterial(DirectMeshMaterial *material, ID3D11Device *device)
{
	this->material = *material;
	HRESULT result;

	D3DX11CreateShaderResourceViewFromFile(device, this->material.mapKd.c_str(), NULL, NULL, &diffuseTexture, &result);
	D3DX11CreateShaderResourceViewFromFile(device, this->material.mapKn.c_str(), NULL, NULL, &normalTexture, &result);
	D3DX11CreateShaderResourceViewFromFile(device, this->material.mapKs.c_str(), NULL, NULL, &specularTexture, &result);
	D3DX11CreateShaderResourceViewFromFile(device, this->material.mapKo.c_str(), NULL, NULL, &opacityTexture, &result);
}

void Mesh::CalculateTangentBinormal(DirectVertex vertex1, DirectVertex vertex2, DirectVertex vertex3,
									D3DXVECTOR3& tangent, D3DXVECTOR3& binormal)
{
	float vector1[3], vector2[3];
	float tuVector[2], tvVector[2];
	float den;
	float length;

	vector1[0] = vertex2.position.x - vertex1.position.x;
	vector1[1] = vertex2.position.y - vertex1.position.y;
	vector1[2] = vertex2.position.z - vertex1.position.z;

	vector2[0] = vertex3.position.x - vertex1.position.x;
	vector2[1] = vertex3.position.y - vertex1.position.y;
	vector2[2] = vertex3.position.z - vertex1.position.z;

	// Calculate the tu and tv texture space vectors.
	tuVector[0] = vertex2.texture.x - vertex1.texture.x;
	tvVector[0] = vertex2.texture.y - vertex1.texture.y;

	tuVector[1] = vertex3.texture.x - vertex1.texture.x;
	tvVector[1] = vertex3.texture.y - vertex1.texture.y;

	// Calculate the denominator of the tangent/binormal equation.
	den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]);

	// Calculate the cross products and multiply by the coefficient to get the tangent and binormal.
	tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
	tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
	tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den;

	binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den;
	binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den;
	binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den;

	// Calculate the length of this normal.
	length = sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z));
			
	// Normalize the normal and then store it
	tangent.x = tangent.x / length;
	tangent.y = tangent.y / length;
	tangent.z = tangent.z / length;

	// Calculate the length of this normal.
	length = sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z));
			
	// Normalize the normal and then store it
	binormal.x = binormal.x / length;
	binormal.y = binormal.y / length;
	binormal.z = binormal.z / length;
}

Mesh& Mesh::operator=(const Mesh &rhs)
{
	Copy(rhs);
	return *this;
}

void Mesh::Copy(const Mesh& v)
{
	vertexBuffer = v.vertexBuffer;
	_polycount = v._polycount;
	diffuseTexture = v.diffuseTexture;
	normalTexture = v.normalTexture;
	specularTexture = v.specularTexture;
	opacityTexture = v.opacityTexture;
	material = v.material;
	vertices = v.vertices;
}

void Mesh::Init(void)
{
	vertexBuffer = 0;
	_polycount = 0;
	diffuseTexture = 0;
	normalTexture = 0;
	specularTexture = 0;
	SecureZeroMemory(&material, sizeof(DirectMeshMaterial));
}

void Mesh::ReleaseResources(void)
{
	GameEngine::SafeRelease(normalTexture);
	GameEngine::SafeRelease(diffuseTexture);
	GameEngine::SafeRelease(specularTexture);
	GameEngine::SafeRelease(opacityTexture);
	GameEngine::SafeRelease(vertexBuffer);
}
