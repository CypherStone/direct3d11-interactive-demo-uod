#include "stdafx.h"

Direct3DManager::Direct3DManager(void)
{
	// Basic data initialisation.
	swapChain = 0;
	device = 0;
	deviceContext = 0;
	renderTarget = 0;
	depthBuffer = 0;
	depthState = 0;
	depthView = 0;
	depthStateZOff = 0;
	alphaState = 0;
	alphaStateOff = 0;
	rasterState = 0;
}

Direct3DManager::Direct3DManager(const Direct3DManager& other){ }

Direct3DManager::~Direct3DManager(void){ }

bool Direct3DManager::SetupDirectX11(HWND hWnd, Camera cam, bool vsync, bool fullscreen)
{
	// Setting up DirectX11 while trying to keep compatibility with older graphic cards.
	// It also manages vertical sync and full screen modes.
	vSync = vsync;
	fullScreen = fullscreen;

	HRESULT result;
	IDXGIFactory* factory;
	IDXGIAdapter* adapter;
	IDXGIOutput* adapterOutput;
	unsigned int numModes, i, numerator = 0, denominator = 0;
	DXGI_MODE_DESC* displayModeList;
	DXGI_ADAPTER_DESC adapterDesc;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ID3D11Texture2D* backBufferPtr;
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	D3D11_RASTERIZER_DESC rasterDesc;
	D3D11_BLEND_DESC blendStateDesc;

	result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
	if(FAILED(result))
	{
		return false;
	}

	result = factory->EnumAdapters(0, &adapter);
	if(FAILED(result))
	{
		return false;
	}

	result = adapter->EnumOutputs(0, &adapterOutput);
	if(FAILED(result))
	{
		return false;
	}

	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
	if(FAILED(result))
	{
		return false;
	}

	displayModeList = new DXGI_MODE_DESC[numModes];
	if(!displayModeList)
	{
		return false;
	}

	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
	if(FAILED(result))
	{
		return false;
	}

	for(i=0; i<numModes; i++)
	{
		if(displayModeList[i].Width == (unsigned int)cam.ScreenWidth())
		{
			if(displayModeList[i].Height == (unsigned int)cam.ScreenHeight())
			{
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}
	}

	// Get the adapter (video card) description.
	result = adapter->GetDesc(&adapterDesc);
	if(FAILED(result))
	{
		return false;
	}

	// Store the dedicated video card memory in megabytes.
	videoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);
	videoCardDescription.assign(adapterDesc.Description, sizeof(adapterDesc.Description));

	// Release the display mode list.
	delete [] displayModeList;
	displayModeList = 0;

	GameEngine::SafeRelease(adapterOutput);
	GameEngine::SafeRelease(adapter);
	GameEngine::SafeRelease(factory);

	// Initialize the swap chain description.
    ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

	// Set to a single back buffer.
    swapChainDesc.BufferCount = 1;

	// Set the width and height of the back buffer.
	swapChainDesc.BufferDesc.Width = cam.ScreenWidth();
	swapChainDesc.BufferDesc.Height = cam.ScreenHeight();

	// Set regular 32-bit surface for the back buffer.
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	// Set the refresh rate of the back buffer.
	if(vSync)
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
	}
	else
	{
	    swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	}

	// Set the usage of the back buffer.
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	// Set the handle for the window to render to.
    swapChainDesc.OutputWindow = hWnd;

	// Turn multisampling off.
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;

	// Set to full screen or windowed mode.
	if(fullscreen)
		swapChainDesc.Windowed = false;
	else
		swapChainDesc.Windowed = true;

	// Set the scan line ordering and scaling to unspecified.
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	// Discard the back buffer contents after presenting.
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	// Don't set the advanced flags.
	swapChainDesc.Flags = 0;

	// Set the feature level to DirectX 11.
	D3D_FEATURE_LEVEL featureLevel[6] = 
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1,
	};
    
	D3D_FEATURE_LEVEL getFeatureLevel;
	result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, featureLevel, 6, D3D11_SDK_VERSION, &swapChainDesc, &swapChain, &device, &getFeatureLevel, &deviceContext);
	
	if(FAILED(result))
		return FALSE;

	switch(getFeatureLevel)
	{
		case D3D_FEATURE_LEVEL_11_0:
				currentLevel = L"Direct3D Version: 11.0";
			break;
		case D3D_FEATURE_LEVEL_10_1:
				currentLevel = L"Direct3D Version: 10.1";
			break;
		case D3D_FEATURE_LEVEL_10_0:
				currentLevel = L"Direct3D Version: 10.0";
			break;
		case D3D_FEATURE_LEVEL_9_3:
				currentLevel = L"Direct3D Version: 9.3";
			break;
		case D3D_FEATURE_LEVEL_9_2:
				currentLevel = L"Direct3D Version: 9.2";
			break;
		case D3D_FEATURE_LEVEL_9_1:
				currentLevel = L"Direct3D Version: 9.1";
			break;
	}

	// Get the pointer to the back buffer.
	result = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPtr);
	if(FAILED(result))
	{
		return false;
	}

	// Create the render target view with the back buffer pointer.
	result = device->CreateRenderTargetView(backBufferPtr, NULL, &renderTarget);
	if(FAILED(result))
	{
		return false;
	}

	// Release pointer to the back buffer as we no longer need it.
	GameEngine::SafeRelease(backBufferPtr);

	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

	depthBufferDesc.Width = cam.ScreenWidth();
	depthBufferDesc.Height = cam.ScreenHeight();
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	result = device->CreateTexture2D(&depthBufferDesc, NULL, &depthBuffer);
	if(FAILED(result))
	{
		return false;
	}

	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.DepthEnable = TRUE;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthStencilDesc.StencilEnable = TRUE;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;

	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	result = device->CreateDepthStencilState(&depthStencilDesc, &depthState);
	if(FAILED(result))
	{
		return false;
	}

	deviceContext->OMSetDepthStencilState(depthState, 1);

	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	result = device->CreateDepthStencilView(depthBuffer, &depthStencilViewDesc, &depthView);
	if(FAILED(result))
	{
		return false;
	}

	deviceContext->OMSetRenderTargets(1, &renderTarget, depthView);

	// Setup the raster description which will determine how and what polygons will be drawn.
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	result = device->CreateRasterizerState(&rasterDesc, &rasterState);
	if(FAILED(result))
	{
		return false;
	}

	// Now set the rasterizer state.
	deviceContext->RSSetState(rasterState);

	D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;

	ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));

	depthDisabledStencilDesc.DepthEnable = false;
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	result = device->CreateDepthStencilState(&depthDisabledStencilDesc, &depthStateZOff);
	if(FAILED(result))
	{
		return false;
	}

	ZeroMemory(&blendStateDesc, sizeof(D3D11_BLEND_DESC));

	//// Create an alpha enabled blend state description.
	blendStateDesc.RenderTarget[0].BlendEnable = TRUE;
    //blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    blendStateDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    blendStateDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
    blendStateDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendStateDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blendStateDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendStateDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	// Creating the alpha blend state
	result = device->CreateBlendState(&blendStateDesc, &alphaState);
	if(FAILED(result))
	{
		return false;
	}

	blendStateDesc.RenderTarget[0].BlendEnable = FALSE;

	result = device->CreateBlendState(&blendStateDesc, &alphaStateOff);
	if(FAILED(result))
	{
		return false;
	}

	return true;
}

void Direct3DManager::BeginRender(D3DXVECTOR4 colour)
{
	// Clear the back buffer.
	deviceContext->ClearRenderTargetView(renderTarget, colour);
	// Clear the depth buffer.
	deviceContext->ClearDepthStencilView(depthView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void Direct3DManager::EndRender(void)
{
	// Swap the backbuffer to the frontbuffer.
	if(vSync)
		swapChain->Present(1, 0);
	else
		swapChain->Present(0, 0);
}

void Direct3DManager::EnableZBuffer(void)
{
	deviceContext->OMSetDepthStencilState(depthState, 1);
}

void Direct3DManager::DisableZBuffer(void)
{
	deviceContext->OMSetDepthStencilState(depthStateZOff, 1);
}

void Direct3DManager::EnableTransparency(void)
{
	float blendFactor[4];

	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;

	deviceContext->OMSetBlendState(alphaState, blendFactor, 0xffffffff);
}

void Direct3DManager::DisableTransparency(void)
{
	float blendFactor[4];

	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	
	deviceContext->OMSetBlendState(alphaStateOff, blendFactor, 0xffffffff);
}

void Direct3DManager::SetFullscreen(void)
{
	if(swapChain)
	{
		fullScreen = !fullScreen;
		swapChain->SetFullscreenState(fullScreen, NULL);
	}
}

void Direct3DManager::ReleaseResources(void)
{
	// Make sure to return out of fullscreen.
	if(swapChain)
	{
		swapChain->SetFullscreenState(FALSE, NULL);
	}

    // Release all COM Objects.
	GameEngine::SafeRelease(depthStateZOff);
	GameEngine::SafeRelease(depthBuffer);
	GameEngine::SafeRelease(depthState);
	GameEngine::SafeRelease(rasterState);
	GameEngine::SafeRelease(depthView);
	GameEngine::SafeRelease(swapChain);
	GameEngine::SafeRelease(renderTarget);
	GameEngine::SafeRelease(device);
	GameEngine::SafeRelease(deviceContext);
	GameEngine::SafeRelease(alphaState);
	GameEngine::SafeRelease(alphaStateOff);
}