#include "stdafx.h"

DirectSoftShadowShader::DirectSoftShadowShader(void)
{
	vertexShader = 0;
	pixelShader = 0;
	layout = 0;
	sampleStateWrap = 0;
	sampleStateClamp = 0;
	matrixBuffer = 0;
	lightBuffer = 0;
	lightBuffer2 = 0;
	materialBuffer = 0;
}

DirectSoftShadowShader::DirectSoftShadowShader(const DirectSoftShadowShader& other) { }

DirectSoftShadowShader::~DirectSoftShadowShader(void) { }

bool DirectSoftShadowShader::SetupShader(ID3D11Device* device, char* vShader, char* pShader)
{
	HRESULT result;
	ID3D10Blob* errorMessage;
	ID3D10Blob* vertexShaderBuffer;
	ID3D10Blob* pixelShaderBuffer;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[5];
	unsigned int numElements;
    D3D11_SAMPLER_DESC samplerDesc;
	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_BUFFER_DESC lightBufferDesc;
	D3D11_BUFFER_DESC lightBufferDesc2;
	D3D11_BUFFER_DESC materialBufferDesc;

	errorMessage = 0;
	vertexShaderBuffer = 0;
	pixelShaderBuffer = 0;

	result = D3DX11CompileFromFile("SoftShadowShader.vs", NULL, NULL, "SoftShadowVertexShader", vShader, D3D10_SHADER_ENABLE_STRICTNESS, 0, NULL, &vertexShaderBuffer, &errorMessage, NULL);
	if(FAILED(result))
	{
		return false;
	}

	result = D3DX11CompileFromFile("SoftShadowShader.ps", NULL, NULL, "SoftShadowPixelShader", pShader, D3D10_SHADER_ENABLE_STRICTNESS, 0, NULL, &pixelShaderBuffer, &errorMessage, NULL);
	if(FAILED(result))
	{
		return false;
	}

    result = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &vertexShader);
	if(FAILED(result))
	{
		return false;
	}

    result = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &pixelShader);
	if(FAILED(result))
	{
		return false;
	}

	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "NORMAL";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;

	polygonLayout[3].SemanticName = "TANGENT";
	polygonLayout[3].SemanticIndex = 0;
	polygonLayout[3].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[3].InputSlot = 0;
	polygonLayout[3].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[3].InstanceDataStepRate = 0;

	polygonLayout[4].SemanticName = "BINORMAL";
	polygonLayout[4].SemanticIndex = 0;
	polygonLayout[4].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[4].InputSlot = 0;
	polygonLayout[4].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[4].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[4].InstanceDataStepRate = 0;

    numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);
	result = device->CreateInputLayout(polygonLayout, numElements, vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), &layout);
	if(FAILED(result))
	{
		return false;
	}

	GameEngine::SafeRelease(vertexShaderBuffer);
	GameEngine::SafeRelease(pixelShaderBuffer);

    samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.MipLODBias = 0.0f;
    samplerDesc.MaxAnisotropy = 1;
    samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    samplerDesc.BorderColor[0] = 1;
	samplerDesc.BorderColor[1] = 1;
	samplerDesc.BorderColor[2] = 1;
	samplerDesc.BorderColor[3] = 1;
    samplerDesc.MinLOD = 0;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
    result = device->CreateSamplerState(&samplerDesc, &sampleStateWrap);
	if(FAILED(result))
	{
		return false;
	}

	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.MipLODBias = 1.0f;
    samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    samplerDesc.BorderColor[0] = 1;
	samplerDesc.BorderColor[1] = 1;
	samplerDesc.BorderColor[2] = 1;
	samplerDesc.BorderColor[3] = 1;
    samplerDesc.MinLOD = 1;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
    result = device->CreateSamplerState(&samplerDesc, &sampleStateClamp);
	if(FAILED(result))
	{
		return false;
	}

    matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
    matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;
	result = device->CreateBuffer(&matrixBufferDesc, NULL, &matrixBuffer);
	if(FAILED(result))
	{
		return false;
	}

	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(LightBufferType);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;
	result = device->CreateBuffer(&lightBufferDesc, NULL, &lightBuffer);
	if(FAILED(result))
	{
		return false;
	}

	lightBufferDesc2.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc2.ByteWidth = sizeof(CameraBuffer);
	lightBufferDesc2.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc2.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc2.MiscFlags = 0;
	lightBufferDesc2.StructureByteStride = 0;
	result = device->CreateBuffer(&lightBufferDesc2, NULL, &lightBuffer2);
	if(FAILED(result))
	{
		return false;
	}

	materialBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	materialBufferDesc.ByteWidth = sizeof(MaterialBuffer);
	materialBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	materialBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	materialBufferDesc.MiscFlags = 0;
	materialBufferDesc.StructureByteStride = 0;
	result = device->CreateBuffer(&materialBufferDesc, NULL, &materialBuffer);
	if(FAILED(result))
	{
		return false;
	}

	return true;
}

bool DirectSoftShadowShader::Render(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
								   D3DXMATRIX projectionMatrix, Mesh *mesh, std::vector<DirectLight> *lights, D3DXVECTOR3 camera, D3DXVECTOR2 fog, D3DXCOLOR fogColour)
{
	HRESULT result;
    D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	LightBufferType* dataPtr2;
	CameraBuffer* dataPtr3;
	MaterialBuffer* matBuff;

	D3DXMatrixTranspose(&worldMatrix, &worldMatrix);
	D3DXMatrixTranspose(&viewMatrix, &viewMatrix);
	D3DXMatrixTranspose(&projectionMatrix, &projectionMatrix);

	result = deviceContext->Map(matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		return false;
	}

	dataPtr = (MatrixBufferType*)mappedResource.pData;
	dataPtr->world = worldMatrix;
	dataPtr->view = viewMatrix;
	dataPtr->projection = projectionMatrix;
    deviceContext->Unmap(matrixBuffer, 0);
    deviceContext->VSSetConstantBuffers(0, 1, &matrixBuffer);

	ID3D11ShaderResourceView *diffuseT = mesh->GetDiffuse();
	deviceContext->PSSetShaderResources(0, 1, &diffuseT);
	ID3D11ShaderResourceView *normalT = mesh->GetNormal();
	deviceContext->PSSetShaderResources(1, 1, &normalT);
	ID3D11ShaderResourceView *specularT = mesh->GetSpecular();
	deviceContext->PSSetShaderResources(2, 1, &specularT);
	ID3D11ShaderResourceView *opacityT = mesh->GetOpacity();
	deviceContext->PSSetShaderResources(3, 1, &opacityT);

	for(int light = 0; light < (int)lights->size(); light++)
	{
		ID3D11ShaderResourceView *map = lights->at(light).GetShadowMap()->GetShaderResourceView();
		deviceContext->PSSetShaderResources(4 + light, 1, &map);
	}

	result = deviceContext->Map(lightBuffer2, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		return false;
	}

	dataPtr3 = (CameraBuffer*)mappedResource.pData;
	dataPtr3->camera = camera;
	dataPtr3->fogBuffer = fog;
	dataPtr3->padding = D3DXVECTOR3(fog.x, fog.y, 0);
		deviceContext->Unmap(lightBuffer2, 0);
		deviceContext->VSSetConstantBuffers(1, 1, &lightBuffer2);

	result = deviceContext->Map(lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		return false;
	}

	dataPtr2 = (LightBufferType*)mappedResource.pData;
	
	for(int l = 0; l < (int)lights->size(); l++)
	{
		dataPtr2->lightAmbient[l] = lights->at(l).GetAmbient();
		dataPtr2->lightDiffuse[l] = lights->at(l).GetDiffuse();
		dataPtr2->lightSpecular[l] = lights->at(l).GetSpecular();
		dataPtr2->lightOn[l].x = lights->at(l).LightOn() ? 1.0f : 0.0f;
		dataPtr2->lightPosition[l] = lights->at(l).GetPosition();
		dataPtr2->lightSpecularPower[l] = lights->at(l).GetSpecularPower();
	}

	deviceContext->Unmap(lightBuffer, 0);
	deviceContext->PSSetConstantBuffers(0, 1, &lightBuffer);

	result = deviceContext->Map(materialBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		return false;
	}

	matBuff = (MaterialBuffer*)mappedResource.pData;
	matBuff->ambient = mesh->GetMaterial()->Ka;
	matBuff->diffuse = mesh->GetMaterial()->Kd;
	matBuff->specular = mesh->GetMaterial()->Ks;
	matBuff->fogColor = fogColour;
	matBuff->specularPower = mesh->GetMaterial()->Ni;
	matBuff->alpha = mesh->GetMaterial()->alpha;
	matBuff->padding = D3DXVECTOR2(0, 0);

	deviceContext->Unmap(materialBuffer, 0);
	deviceContext->PSSetConstantBuffers(1, 1, &materialBuffer);

	RenderShader(deviceContext, mesh->PolygonCount());

	return true;
}

void DirectSoftShadowShader::RenderShader(ID3D11DeviceContext* deviceContext, int vertexCount)
{
	deviceContext->IASetInputLayout(layout);
    deviceContext->VSSetShader(vertexShader, NULL, 0);
    deviceContext->PSSetShader(pixelShader, NULL, 0);
	deviceContext->PSSetSamplers(0, 1, &sampleStateClamp);
	deviceContext->PSSetSamplers(1, 1, &sampleStateWrap);
	deviceContext->Draw(vertexCount, 0);
}

void DirectSoftShadowShader::ReleaseResources(void)
{
	GameEngine::SafeRelease(lightBuffer);
	GameEngine::SafeRelease(lightBuffer2);
	GameEngine::SafeRelease(materialBuffer);
	GameEngine::SafeRelease(matrixBuffer);
	GameEngine::SafeRelease(sampleStateWrap);
	GameEngine::SafeRelease(sampleStateClamp);
	GameEngine::SafeRelease(layout);
	GameEngine::SafeRelease(pixelShader);
	GameEngine::SafeRelease(vertexShader);
}
